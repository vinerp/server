﻿using NUnit.Framework;
using VineRP.Modules.Util;

namespace VineRP.Tests
{
    [TestFixture]
    public class SscanfTest
    {
        [Test]
        public void StringParsing()
        {
            var testString = "hello world";
            testString.sscanf("ss", out object[] args);
            var firstString = (string) args[0];
            var lastString = (string) args[1];

            Assert.AreEqual("hello", firstString);
            Assert.AreEqual("world", lastString);
        }

        [Test]
        public void SentenceParsing()
        {
            var testSentence = "hello dear world";
            testSentence.sscanf("ss", out object[] args);
            var firstWord = (string) args[0];
            var sentence = (string) args[1];

            Assert.AreEqual("hello", firstWord);
            Assert.AreEqual("dear world", sentence);
        }
    }
}