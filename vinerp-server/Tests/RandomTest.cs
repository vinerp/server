﻿using NUnit.Framework;

namespace VineRP.Tests
{
    [TestFixture]
    public class RandomTest
    {
        [Test]
        [Repeat(10)]
        public void RandomInteger()
        {
            var randomInteger = Random.GetInt(10, 20);

            Assert.That(randomInteger, Is.AtLeast(10));
            Assert.That(randomInteger, Is.LessThan(20));
        }

        [Test]
        [Repeat(10)]
        public void RandomFloat()
        {
            var randomFloat = Random.GetFloat(0, 10f);

            Assert.That(randomFloat, Is.AtLeast(0f));
            Assert.That(randomFloat, Is.LessThan(10f));
        }
    }
}