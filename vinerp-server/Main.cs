﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Timers;
using CryptSharp;
using GTANetworkServer;
using GTANetworkShared;
using VineRP.Modules.Server.Commands;
using VineRP.Modules.Util;
using CryptSharp;
using VineRP.Modules.Server;
using Timer = System.Timers.Timer;

namespace VineRP
{
    public class Main : Script
    {
        public static Main Instance;
        /* Managers */
        private CommandManager _commandManager;
        private AuthenticationManager _authenticationManager;

        public const LogLevel logLevel = LogLevel.All;

        public Main()
        {
            Instance = this;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;

            API.onResourceStart += () =>
            {
                API.setServerPassword("init" + Random.GetInt(0, 200000));
                Console.BackgroundColor = ConsoleColor.White;
                Console.ForegroundColor = ConsoleColor.Black;
                API.consoleOutput("==========================");
                API.consoleOutput("      Starting VineRP     ");
                API.consoleOutput("==========================");
                Console.ResetColor();

                API.setGamemodeName($"{GlobalSettings.SERVER_GAMEMODE_NAME} v{GlobalSettings.SERVER_GAMEMODE_VERSION}");
                API.setServerName(GlobalSettings.SERVER_NAME);

                _commandManager = new CommandManager(this);
                _authenticationManager = new AuthenticationManager();

                API.requestIpl("RC12B_HospitalInterior");
                API.requestIpl("Coroner_Int_on");
                API.removeIpl("farmint_cap");

                #region MySQL
                var database = new Database();
                if (database.Connect(GlobalSettings.MYSQL_HOSTNAME, GlobalSettings.MYSQL_USERNAME, GlobalSettings.MYSQL_PASSWORD, GlobalSettings.MYSQL_DATABASE))
                {
                    Log.Success("Connected to MySQL successfully");
                }
                else
                {
                    Log.Error("Failed to connect to a MySQL database");
                }
                #endregion

                Door.LoadAll();
                VehicleDealership.Init();
                Faction.InitJobs();
                Item.Init();
                EconomyManager.Init();
                PlayerUI.Init();
                Menu.Init();
                VehicleRent.Init();

                CreateBlips();

                Log.Info("Starting the weather updater...");
                var timer = new Timer(1000 * 30) {AutoReset = true};
                timer.Elapsed += WeatherUpdateTimer;
                timer.Start();

                Log.Success($"{GlobalSettings.SERVER_GAMEMODE_NAME} v{GlobalSettings.SERVER_GAMEMODE_VERSION} is loaded.");
                
                API.setServerPassword("test123");
            };

            API.onResourceStop += () =>
            {
                foreach (var player in Player.All)
                {
                    player.Save();
                    player.Client.kick("server is shutting down");
                }

                FactionPolice.SaveTickets();

                Log.Info("Starting shutdown cleanup.");
                if (Database.GetInstance().IsConnected())
                {
                    Log.Info("Cleaning up the MySQL instance");
                    Database.GetInstance().Close();
                }
                Log.Info("Shutdown cleanup complete.");
                Log.Info("VineRP is shutting down.");
            };

            API.onChatCommand += (sender, command, cancel) =>
            {
                var player = Player.GetPlayerFromClient(sender);
                cancel.Cancel = true;
                if (!player.IsLoggedIn && !(command.StartsWith("/login")) && !(command.StartsWith("/register")))
                {
                    player.SendErrorMessage("You must be logged in to execute commands.");
                    return;
                }
                if (!_commandManager.Process(command, player))
                {
                    player.SendErrorMessage("Command not found.");
                }
            };

            API.onChatMessage += (sender, message, cancel) =>
            {
                cancel.Cancel = true;
                Player.GetPlayerFromClient(sender)?.OnChatMessage(message);
            };

            API.onClientEventTrigger += (player, eventname, args) =>
            {
                Player.GetPlayerFromClient(player)?.OnEventTrigger(eventname, args);
            };

            API.onUpdate += () =>
            {
                
            };

            /* Player events */

            API.onPlayerConnected += player =>
            {
                //var pl = new Player(player);
                //pl.OnConnected();
                //Player.All.Add(pl);
                Player.GetPlayerFromClient(player)?.OnConnected();
            };

            API.onPlayerBeginConnect += (player, connection) =>
            {
                foreach (var spl in Player.All)
                {
                    if (player.address == spl.Client.address)
                    {
                        player.sendChatMessage($"Error: Someone is already connected with this ip. ({player.address})");
                        player.kick("IP Taken");
                        break;
                    }
                }

                var pl = new Player(player);
                pl.AssignUniqueId();
                pl.OnBeginConnect();
                Player.All.Add(pl);
            };

            API.onPlayerRespawn += player =>
            {
                Player.GetPlayerFromClient(player)?.OnRespawn();
            };

            API.onPlayerDisconnected += (player, reason) =>
            {
                var pl = Player.All.FindIndex(p => p.Client.name == player.name);
                if (pl != -1)
                {
                    Log.Debug("Before release: " + Player.TakenPlayerIds.Count);
                    Player.All[pl].OnDisconnected(reason);
                    Player.TakenPlayerIds.Remove(Player.All[pl].ID);
                    Log.Debug("After release: " + Player.TakenPlayerIds.Count);
                    Player.All.RemoveAt(pl);
                }
                else
                {
                    throw new Exception($"Invalid index found when disconnecting a player. Name ({player.name}) SC ({player.socialClubName})");   
                }
            };

            API.onPlayerFinishedDownload += player =>
            {
                Player.GetPlayerFromClient(player)?.OnFinishedDownload();
            };

            API.onEntityEnterColShape += (colshape, entity) =>
            {
                if (API.getEntityType(entity) == EntityType.Player)
                {
                    Player.GetPlayerFromNetHandle(entity)?.OnEnterColShape(colshape);
                }
            };

            API.onPlayerDeath += (player, killer, weapon) =>
            {
                Player.GetPlayerFromClient(player)?.OnDeath(killer, weapon);
            };

            API.onPlayerEnterVehicle += (player, vehicle) =>
            {
                /* Delay onplayerentervehicle until seat incosistency is fixed */
                var veh = Vehicle.GetVehicleFromNetHandle(vehicle);
                if (veh == null)
                {
                    Log.Debug("Got null whilst trying to get vehicle for nethandle @ onPlayerEnterVehicle");
                }
                else
                {
                    /* Delay calling the event so that the seat ids are updated correctly */
                    var timer = new Timer
                    {
                        AutoReset = false,
                        Interval = 750
                    };
                    timer.Start();

                    timer.Elapsed += (sender, args) =>
                    {
                        Player.GetPlayerFromClient(player)?.OnEnterVehicle(veh);
                    };
                }
            };

            API.onPlayerExitVehicle += (client, vehicle) =>
            {
                var veh = Vehicle.GetVehicleFromNetHandle(vehicle);
                var player = Player.GetPlayerFromClient(client);
                if (veh == null)
                {
                    Log.Debug("Got null whilst trying to get vehicle for nethandle @ onPlayerExitVehicle");
                }
                else if (player == null)
                {
                    Log.Debug("Got null whilst trying to get player for nethandle @ onPlayerExitVehicle");
                }
                else
                {
                    player.OnExitVehicle(veh);
                }
            };

            /* Vehicle events */
            API.onVehicleDeath += entity =>
            {
                Vehicle.GetVehicleFromNetHandle(entity)?.OnDeath();

                if(entity == null) Log.Debug("died vehicle is null");
                Log.Debug($"Vehicle of handle {entity.Value} has died.");
            };

            API.onVehicleHealthChange += (entity, value) =>
            {
                Vehicle.GetVehicleFromNetHandle(entity)?.OnHealthChange(value);
            };
        }

        private static void WeatherUpdateTimer(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            DateTimeOffset utcOffset = new DateTimeOffset(DateTime.Now).ToUniversalTime();
            API.shared.setTime(utcOffset.Hour, utcOffset.Minute);
        }

        [Flags]
        public enum LogLevel
        {
            None = 0,
            Error = 1,
            Warnings = 2,
            Debug = 4,
            All = Error | Warnings | Debug
        }

        private static void CreateBlips()
        {
            /* Car dealership */
            var blip = API.shared.createBlip(new Vector3(-42.55413, -1092.99, 26.42235));
            API.shared.setBlipSprite(blip, 225);
            GTANetworkServer.API.shared.setBlipColor(blip, (int)BlipColor.LightGreen);

            /* Banks */
            foreach (var bankLocation in EconomyManager.BankLocations)
            {
                blip = API.shared.createBlip(bankLocation);
                API.shared.setBlipSprite(blip, 108);
                API.shared.setBlipColor(blip, (int)BlipColor.DarkGreen); // Dark green
            }

            /* Airport vehicle rent parking lot */
            blip = GTANetworkServer.API.shared.createBlip(new Vector3(-1043.976, -2660.698, 13.40652));
            GTANetworkServer.API.shared.setBlipSprite(blip, 225);

            /* Trucking faction */
            blip = GTANetworkServer.API.shared.createBlip(Faction.Trucking.JoinPosition);
            GTANetworkServer.API.shared.setBlipSprite(blip, 477);

            /* Garbage faction */
            blip = GTANetworkServer.API.shared.createBlip(Faction.Garbage.JoinPosition);
            GTANetworkServer.API.shared.setBlipSprite(blip, 318);
        }
    }
}
