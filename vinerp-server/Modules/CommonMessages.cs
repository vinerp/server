﻿namespace VineRP
{
    public static class CommonMessages
    {
        public const string PLAYER_NOT_FOUND = "Player was not found.";
        public const string NOT_IN_FACTION = "You must be in a faction to use this.";
        public const string NOT_IN_VEHICLE = "You are not in a vehicle";
        public const string NOT_AUTHORISED = "You are not authorised to do this.";
    }
}