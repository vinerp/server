﻿namespace VineRP
{
    public sealed class Random
    {
        private static System.Random _randomInstance = null;

        private static void CheckRandomInstance()
        {
            if(_randomInstance == null) _randomInstance = new System.Random();
        }

        public static int GetInt(int from, int to)
        {
            CheckRandomInstance();
            return _randomInstance.Next(from, to);
        }

        public static float GetFloat(float from, float to)
        {
            CheckRandomInstance();
            return (float)_randomInstance.NextDouble() * (to - from) + from;
        }
    }
}
