﻿namespace VineRP
{
    public static class VKeys
    {
        public const int COMMA = 188;
        public const int PERIOD = 190;
        public const int F1 = 112;
        public const int F2 = 113;
        public const int ENTER = 13;
        public const int CTRL = 17;
        public const int SHIFT = 16;
        public const int SPACE = 32;
        public const int TAB = 9;

        public const int O = 79;
        public const int GRAVE = 223;
    }
}