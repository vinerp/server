﻿using System;
using System.Timers;

namespace VineRP
{
    /* Purpose of this class is to wrap the original timer class + provide support for attaching players to the timer
       this prevents the timer from being executed when the attached player disconnects thus prevent nullpointers.
    */
    public class VTimer
    {
        private Timer Timer;
        public EventHandler<ElapsedEventArgs> Elapsed;
        public Player Player;
        private bool playerRelationshipAdded;

        public VTimer(double interval, bool autoReset = true)
        {
            Timer = new Timer(interval)
            {
                AutoReset = autoReset
            };

            Timer.Elapsed += TimerOnElapsed;
        }

        public void AddPlayerRelationship(Player player)
        {
            Player = player;
            playerRelationshipAdded = true;
        }

        public void Start()
        {
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Stop();
        }

        public void Dispose()
        {
            Timer.Stop();
            Timer.Dispose();
        }

        public bool IsStarted()
        {
            return Timer.Enabled;
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            if (playerRelationshipAdded)
            {
                if (Player == null) return;
            }
            Elapsed(this, elapsedEventArgs);
        }
    }
}