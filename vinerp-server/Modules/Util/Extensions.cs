﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VineRP.Modules.Util
{
    public static class Extensions
    {

        public static bool sscanf(this string formatString, string format, out object[] formatParams)
        {
            formatParams = new object[format.Length];
            if (format.Length == 0) return false;
            if (formatString.Length == 0) return false;
            var parameters = formatString.Split(' ');

            if (parameters.Length < format.Length)
                return false;

            for (var i = 0; i < format.Length; i++)
            {
                if (format[i] == 's')
                {
                    if (i == format.Length-1) // if is last format attr
                    {
                        formatParams[i] = (string)"";
                        var iteration = 0;
                        for (int p = i; p < parameters.Length; p++)
                        {
                            if (iteration > 0) formatParams[i] += " ";
                            formatParams[i] += (string) "" + parameters[p];
                            iteration++;
                        }
                    }
                    else
                    {
                        formatParams[i] = (string) parameters[i];
                    }
                }
                else if (format[i] == 'i')
                {
                    if (!int.TryParse(parameters[i], out int intRes))
                        return false;
                    formatParams[i] = intRes;
                }
                else if (format[i] == 'c')
                {
                    if (!char.TryParse(parameters[i], out char charRes))
                        return false;
                    formatParams[i] = charRes;
                }
                else if (format[i] == 'f')
                {
                    if (!float.TryParse(parameters[i], out float floatRes))
                        return false;
                    formatParams[i] = floatRes;
                }
                else if (format[i] == 'u')
                {
                    formatParams[i] = Player.Get(parameters[i]);
                }
            }
            return true;
        }
    }
}
