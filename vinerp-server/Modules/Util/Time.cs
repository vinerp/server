﻿using System;

namespace VineRP
{
    public static class Time
    {
        public static int GetUnixTimestamp()
        {
            return (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        public static int DateTimeToUnixTimestamp(DateTime dateTime)
        {
            var unixDate = new DateTime(1970, 1, 1, 0, 0, 0);
            return (int)dateTime.Subtract(unixDate).TotalSeconds;
        }

        public static DateTime UnixTimestampToDateTime(int unixSeconds)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(unixSeconds);
        }
    }
}