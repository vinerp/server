﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;

namespace VineRP
{
    class Log
    {
        public static void log(string message)
        {
            API.shared.consoleOutput(message);
        }

        public static void Debug(string message)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"[{DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss")}] DEBUG: {message}");
            Console.ResetColor();
        }

        public static void Warning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"[{DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss")}] WARNING: {message}");
            Console.ResetColor();
        }

        public static void Info(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine($"[{DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss")}] INFO: {message}");
            Console.ResetColor();
        }

        public static void Custom(string tag, ConsoleColor color, string message)
        {
            Console.ForegroundColor = color;
            Console.WriteLine($"[{DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss")}] {tag}: {message}");
            Console.ResetColor();
        }

        public static void Success(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"[{DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss")}] SUCCESS: {message}");
            Console.ResetColor();
        }

        public static void Error(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"[{DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss")}] ERROR: {message}");
            Console.ResetColor();
        }
    }
}
