﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VineRP
{
    public class Inventory
    {
        public const int BASE_PLAYER_SLOTS = 5;
        private int MaxSlots;

        private List<Tuple<Item, dynamic>> Items = new List<Tuple<Item, dynamic>>();

        public Inventory(int maxSlots)
        {
            MaxSlots = maxSlots;
        }

        /// <summary>
        /// Take an item from the inventory
        /// </summary>
        public bool Take(Item item, dynamic quantity)
        {
            if(!HasItem(item)) return false;
            int itemIndex = Items.FindIndex(i => i.Item1 == item);
            if (GetItemQuantity(item) <= quantity)
            {
                // Remove the item
                Items.RemoveAt(itemIndex);
            }
            else
            {
                Items[itemIndex] = new Tuple<Item, dynamic>(Items[itemIndex].Item1, Items[itemIndex].Item2-quantity);
            }
            return true;
        }

        /// <summary>
        /// Does the inventory have space for a specific item.
        /// </summary>
        public bool HasSpaceForItem(Item item, dynamic quantity)
        {
            if (!IsFull())
                return true;

            if (HasItem(item))
            {
                if ((GetItemQuantity(item)+quantity) < item.MaxStackableSize)
                {
                    return true;
                }
            }
            return false;
        }

        public List<Tuple<Item, dynamic>> GetAllItems()
        {
            return Items;
        }

        /// <summary>
        /// Add an item to the inventory.
        /// </summary>
        public bool Give(Item item, dynamic quantity, bool force = false)
        {
            if (HasItem(item))
            {
                /* If the inventory already contains item, just add the quantity. */
                int itemIndex = Items.FindIndex(i => i.Item1 == item);
                Items[itemIndex] = new Tuple<Item, dynamic>(Items[itemIndex].Item1, Items[itemIndex].Item2+quantity);
            }
            else
            {
                /* If the inventory is full and item is not forced, deny adding it to the inventory */
                if (IsFull() && !force) return false;
                Items.Add(new Tuple<Item, dynamic>(item, quantity));
            }
            return true;
        }

        /// <summary>
        /// Check if the inventory is full.
        /// </summary>
        /// <returns></returns>
        public bool IsFull()
        {
            return GetUsedSlots() >= MaxSlots;
        }

        /// <summary>
        /// Get the used inventory slots.
        /// </summary>
        public int GetUsedSlots()
        {
            return Items.Count;
        }

        /// <summary>
        /// Check if the inventory contains a specific item.
        /// </summary>
        public bool HasItem(Item item)
        {
            return Items.Exists(i => i.Item1 == item);
        }

        /// <summary>
        /// Get how much of the item the inventory contain.
        /// </summary>
        public dynamic GetItemQuantity(Item item)
        {
            if (!HasItem(item)) return 0;
            int itemIndex = Items.FindIndex(i => i.Item1 == item);
            return Items[itemIndex].Item2;
        }
    }
}
