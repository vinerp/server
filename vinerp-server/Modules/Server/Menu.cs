﻿using System;
using System.Collections.Generic;
using GTANetworkServer;

namespace VineRP
{
    public class Menu
    {
        private Player _player;

        private string _banner;
        private string _title;
        private int _offsetX;
        private int _offsetY;
        private bool _canExit;
        private MenuAnchor _anchor;

        private List<string> _itemLabels = new List<string>();
        private List<string> _itemDescriptions = new List<string>();

        public EventHandler<MenuItemSelectArgs> OnItemSelect;
        public EventHandler<EventArgs> OnMenuClose;

        public static void Init()
        {
            Log.Info("Initialising menus...");

            Main.Instance.API.onClientEventTrigger += ClientEventTrigger;
        }

        private static void ClientEventTrigger(Client sender, string eventName, params object[] args)
        {
            var player = Player.GetPlayerFromClient(sender);
            if (player == null)
            {
                throw new Exception("Got null when trying to resolve a player from client @ Menu.ClientEventTrigger");
            }

            if (eventName == PlayerEvents.MENU_ONITEMSELECT)
            {
                player.CurrentMenu?.ItemSelected((int)args[0]);
            }
            else if (eventName == PlayerEvents.MENU_ONCLOSE)
            {
                player.CurrentMenu?.MenuClosed();
            }
        }

        public Menu(Player player, int x, int y, MenuAnchor anchor, string banner = "", string title = "", bool canExit = true)
        {
            _player = player;
            _offsetX = x;
            _offsetY = y;
            _anchor = anchor;
            _banner = banner;
            _title = title;
            _canExit = canExit;
        }

        /// <summary>
        /// Add an item to the menu
        /// </summary>
        public void AddItem(string label, string description)
        {
            _itemLabels.Add(label);
            _itemDescriptions.Add(description);
        }

        private void ItemSelected(int index)
        {
            var menuItemSelectArgs = new MenuItemSelectArgs();
            menuItemSelectArgs.Player = _player;
            menuItemSelectArgs.Index = index;

            OnItemSelect?.Invoke(this, menuItemSelectArgs);
        }

        private void MenuClosed()
        {
            OnMenuClose?.Invoke(this, new EventArgs());
        }

        public void Close()
        {
            // TODO: Close the menu.
        }

        /// <summary>
        /// Show the menu to the player.
        /// </summary>
        public void Show()
        {
            _player.TriggerEvent(PlayerEvents.MENU_SHOW, _banner, _title, _offsetX, _offsetY, (int)_anchor, _canExit, _itemLabels.ToArray(), _itemDescriptions.ToArray());
            _player.CurrentMenu = this;
        }
    }

    public class MenuItemSelectArgs
    {
        public Player Player;
        public int Index;
    }

    public enum MenuAnchor
    {
        TopLeft = 0,
        TopMiddle = 1,
        TopRight = 2,
        MiddleLeft = 3,
        Middle = 4,
        MiddleRight = 6,
        Bottomleft = 7,
        BottomMiddle = 8,
        BottomRight = 9
    }
}