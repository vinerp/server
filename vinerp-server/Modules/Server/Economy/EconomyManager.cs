﻿using System;
using System.Collections.Generic;
using GTANetworkServer;
using GTANetworkShared;

namespace VineRP
{
    internal static class EconomyManager
    {
        public static readonly List<Vector3> ATMLocations = new List<Vector3>()
        {
            new Vector3(-1109.797f, -1690.808f, 4.375014f),
            new Vector3(-821.6062f, -1081.885f, 11.13243f),
            new Vector3(-537.8409f, -854.5145f, 29.28953f),
            new Vector3(-1315.744f, -834.6907f, 16.96173f),
            new Vector3(-1314.786f, -835.9669f, 16.96015f),
            new Vector3(-1570.069f, -546.6727f, 34.95547f),
            new Vector3(-1571.018f, -547.3666f, 34.95734f),
            new Vector3(-866.6416f, -187.8008f, 37.84286f),
            new Vector3(-867.6165f, -186.1373f, 37.8433f),
            new Vector3(-721.1284f, -415.5296f, 34.98175f),
            new Vector3(-254.3758f, -692.4947f, 33.63751f),
            new Vector3(24.37422f, -946.0142f, 29.35756f),
            new Vector3(130.1186f, -1292.669f, 29.26953f),
            new Vector3(129.7023f, -1291.954f, 29.26953f),
            new Vector3(129.2096f, -1291.14f, 29.26953f),
            new Vector3(288.8256f, -1282.364f, 29.64128f),
            new Vector3(1077.768f, -776.4548f, 58.23997f),
            new Vector3(527.2687f, -160.7156f, 57.08937f),
            new Vector3(-867.5897f, -186.1757f, 37.84291f),
            new Vector3(-866.6556f, -187.7766f, 37.84278f),
            new Vector3(-1205.024f, -326.2916f, 37.83985f),
            new Vector3(-1205.703f, -324.7474f, 37.85942f),
            new Vector3(-1570.167f, -546.7214f, 34.95663f),
            new Vector3(-1571.056f, -547.3947f, 34.95724f),
            new Vector3(-57.64693f, -92.66162f, 57.77995f),
            new Vector3(527.3583f, -160.6381f, 57.0933f),
            new Vector3(-165.1658f, 234.8314f, 94.92194f),
            new Vector3(-165.1503f, 232.7887f, 94.92194f),
            new Vector3(-2072.445f, -317.3048f, 13.31597f),
            new Vector3(-3241.082f, 997.5428f, 12.55044f),
            new Vector3(-1091.462f, 2708.637f, 18.95291f),
            new Vector3(1172.492f, 2702.492f, 38.17477f),
            new Vector3(1171.537f, 2702.492f, 38.17542f),
            new Vector3(1822.637f, 3683.131f, 34.27678f),
            new Vector3(1686.753f, 4815.806f, 42.00874f),
            new Vector3(1701.209f, 6426.569f, 32.76408f),
            new Vector3(-95.54314f, 6457.19f, 31.46093f),
            new Vector3(-97.23336f, 6455.469f, 31.46682f),
            new Vector3(-386.7451f, 6046.102f, 31.50172f),
            new Vector3(-1091.42f, 2708.629f, 18.95568f),
            new Vector3(5.132f, -919.7711f, 29.55953f),
            new Vector3(-660.703f, -853.971f, 24.484f),
            new Vector3(-2293.827f, 354.817f, 174.602f),
            new Vector3(-2294.637f, 356.553f, 174.602f),
            new Vector3(-2295.377f, 358.241f, 174.648f),
            new Vector3(-1409.782f, -100.41f, 52.387f),
            new Vector3(-1410.279f, -98.649f, 52.436f),
            new Vector3(146.0866, -1034.516, 29.34502),
            new Vector3(147.6411, -1035.07, 29.34332)
        };

        public static readonly List<Vector3> BankLocations = new List<Vector3>()
        {
            new Vector3(-351.121, -49.84779, 49.04258),
            new Vector3(149.733, -1040.669, 29.37409)
        };

        public static void Init()
        {
            Log.Info("Starting the economy manager...");

            foreach (var atmLocation in ATMLocations)
            {
                API.shared.createTextLabel("ATM\nUse /atm", atmLocation, 10f, 1f, false);
            }

            Main.Instance.API.onClientEventTrigger += PlayerEventTrigger;
        }

        private static void PlayerEventTrigger(Client sender, string eventName, params object[] args)
        {
            var player = Player.GetPlayerFromClient(sender);
            if (player == null) return;

            if (eventName == PlayerEvents.ECONOMY_BANK_DEPOSIT)
            {
                if (!int.TryParse(args[0].ToString(), out int depositAmount))
                {
                    player.SendErrorMessage("Invalid deposit amount entered.");
                    return;
                }
                if (depositAmount < 1)
                {
                    player.SendErrorMessage("You cannot deposit less than $1.");
                    return;
                }
                if (depositAmount > player.Money)
                {
                    player.SendErrorMessage("You don't have that much money to deposit.");
                    return;
                }
                player.Money -= depositAmount;
                player.BankMoney += depositAmount;
                player.SendSuccessMessage($"You have deposited ${depositAmount}.");
            }
            else if (eventName == PlayerEvents.ECONOMY_BANK_WITHDRAW)
            {
                Log.Error("Arg count " + args.Length);
                if (!int.TryParse(args[0].ToString(), out int withdrawAmount))
                {
                    player.SendErrorMessage("Invalid withdrawal amount entered.");
                    return;
                }
                if (withdrawAmount < 1)
                {
                    player.SendErrorMessage("You cannot withdraw less than $1.");
                    return;
                }
                if (withdrawAmount > player.BankMoney)
                {
                    player.SendErrorMessage("You don't have that much money in your bank account.");
                    return;
                }
                player.Money += withdrawAmount;
                player.BankMoney -= withdrawAmount;
                player.SendSuccessMessage($"You have withdrawn ${withdrawAmount}.");
            }
            else if (eventName == PlayerEvents.ECONOMY_BANK_CHECKBALANCE)
            {
                player.SendInfoMessage($"You currently have ${player.BankMoney} in your back account.");
            }
        }
    }
}
