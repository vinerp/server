﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace VineRP
{
    class Database
    {
        private static Database instance = null;
        private MySqlConnection m_Connection;
        private bool m_IsConnected = false;

        private string m_Server;
        private string m_User;
        private string m_Database;
        private int m_Port;
        private string m_Password;

        public Database()
        {
            instance = this;
        }

        public bool Connect(string server, string user, string password, string db, int port = 3306)
        {
            m_Server = server; m_User = user;
            m_Password = password; m_Database = db;
            m_Port = port;

            string connectionString =
                $"server={m_Server};user={m_User};database={m_Database};port={m_Port};password={m_Password}";
            var connectionBuilder = new MySqlConnectionStringBuilder(connectionString);
            connectionBuilder.Keepalive = UInt32.MaxValue;
            m_Connection = new MySqlConnection(connectionBuilder.GetConnectionString(true));

            try
            {
                m_Connection.Open();
                m_IsConnected = true;
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occured whilst connecting to the MySQL database!");
                Console.WriteLine("Message: " + ex.ToString());
                return false;
            }
        }

        public void Close()
        {
            if (IsConnected()) m_Connection.Close();
        }

        public static Database GetInstance()
        {
            return instance ?? (instance = new Database());
        }

        public MySqlConnection GetConnection()
        {
            return IsConnected() ? m_Connection : null;
        }

        public bool IsConnected()
        {
            return m_IsConnected;
        }
    }

    public class DBQuery
    {
        private string m_Query;
        private QueryType m_QueryType;
        private MySqlCommand m_Command;
        private MySqlDataReader m_Result;
        private object m_ScalarResult;

        public DBQuery(string query, QueryType queryType = QueryType.QUERY)
        {
            m_Query = query;
            m_QueryType = queryType;

            m_Command = new MySqlCommand(m_Query, Database.GetInstance().GetConnection());
        }

        public DBQuery AddParam(string parameterName, object value)
        {
            m_Command.Parameters.AddWithValue(parameterName, value);
            return this;
        }

        public void Close()
        {
            if (m_QueryType == QueryType.QUERY)
                if (m_Result.IsClosed == false)
                    m_Result.Close();
        }

        public MySqlCommand GetCommand()
        {
            return m_Command;
        }

        public MySqlDataReader GetResult()
        {
            return m_Result;
        }

        public object GetScalarResult()
        {
            return m_ScalarResult;
        }

        public MySqlDataReader Execute(bool readFirst = false)
        {
            if (m_QueryType == QueryType.QUERY)
            {
                m_Result = m_Command.ExecuteReader();

                if (readFirst)
                    if (m_Result.HasRows)
                        m_Result.Read();

                return m_Result;
            }
            else
            {
                m_Command.ExecuteNonQuery();
                return null;
            }
        }

        public object ExecuteScalar()
        {
            if (m_QueryType == QueryType.SCALAR)
            {
                m_ScalarResult = m_Command.ExecuteScalar();
                return m_ScalarResult;
            }
            return null;
        }

        public static int GetNumberRowsInTable(string tableName)
        {
            DBQuery query = new DBQuery(string.Format("SELECT COUNT(*) FROM `{0}`", tableName), QueryType.SCALAR);
            int result = Convert.ToInt32(query.ExecuteScalar());
            query.Close();
            return result;
        }
    }

    public enum QueryType
    {
        INSERT_OR_DELETE = 1,
        QUERY = 2,
        SCALAR = 3
    }
}
