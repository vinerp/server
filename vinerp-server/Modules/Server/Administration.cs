﻿using System;
using System.Collections.Generic;
using GTANetworkShared;

namespace VineRP
{
    public static class Administration
    {
        public static readonly List<Tuple<string, Vector3>> Teleports = new List<Tuple<string, Vector3>>()
        {
            new Tuple<string, Vector3>("Trucking base", new Vector3(805.1796, -2941.632, 5.910103)),
            new Tuple<string, Vector3>("Garbage base", new Vector3(1382.565, -2079.832, 51.99852)),
            new Tuple<string, Vector3>("Vehicle dealership", new Vector3(-38.73658, -1100.82, 26.42235))
        };
    }
}
