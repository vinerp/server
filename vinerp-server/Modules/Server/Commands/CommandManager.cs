﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;

namespace VineRP.Modules.Server.Commands
{
    public class CommandManager
    {
        private List<DefaultCommand> commands = new List<DefaultCommand>();

        public CommandManager(Script script)
        {
            RegisterCommands(script.GetType());
        }

        public void RegisterCommands<T>() where T : class
        {
            RegisterCommands(typeof(T));
        }

        public void RegisterCommands(Type typeInAssembly)
        {
            if(typeInAssembly == null) throw new ArgumentNullException(nameof(typeInAssembly));
            RegisterCommands(typeInAssembly.Assembly);
        }

        public void RegisterCommands(Assembly assembly)
        {
            foreach (var method in assembly.GetTypes()
                .Where(type => !type.IsInterface && type.IsClass && !type.IsAbstract)
                .SelectMany(type => type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance))
                .Where(method => method.ReturnType == typeof(bool))
                .Where(method => method.Name.StartsWith("COMMAND_") || method.Name.StartsWith("CMD_"))
            )
            {
                var commandName = method.Name.StartsWith("COMMAND_") ? method.Name.Substring(8, method.Name.Length - 8)
                    : method.Name.Substring(4, method.Name.Length - 4);

                var command = new DefaultCommand(commandName, method);
                if (method.IsStatic) command.IsMethodMemberOfPlayer = false;
                commands.Add(command);
            }
        }

        public DefaultCommand GetCommandFromText(Player player, string text)
        {
            foreach (var command in commands)
            {
                if (command.CanInvoke(player, text))
                    return command;
            }
            return null;
        }

        public bool Process(string commandText, Player player)
        {
            Log.log($"[COMMAND]: {player.Client.name}: {commandText}");
            if (string.IsNullOrWhiteSpace(commandText)) return false;

            var command = GetCommandFromText(player, commandText);
            return command != null && command.Invoke(player, commandText);
        }
    }
}
