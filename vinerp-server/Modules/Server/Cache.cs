﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eastLIFEV.Modules.Server
{
    public class Cache
    {
        private Dictionary<string, object> cache;

        public Cache()
        {
            cache = new Dictionary<string, object>();
        }

        public void Clear()
        {
            cache.Clear();
        }

        public void Remove(string key)
        {
            cache.Remove(key);
        }

        public void Set(string key, object val)
        {
            if (!cache.ContainsKey(key)) cache.Add(key, val);
            else cache[key] = val;
        }

        public object Get(string key)
        {
            if(!cache.ContainsKey(key)) throw new Exception("Trying to get a non-existing cache key.");
            return cache[key];
        }

        public bool Contains(string key)
        {
            return cache.ContainsKey(key);
        }
    }
}
