﻿using GTANetworkShared;

namespace VineRP
{
    public class Blip
    {
        private Player _player;
        private Vector3 _position;
        private int _sprite;
        private BlipColor _color;
        private int _id;

        public bool IsCreated;

        public Blip(Player player, Vector3 position, int sprite = 1, BlipColor color = BlipColor.White)
        {
            _player = player;
            _position = position;
            _sprite = sprite;
            _color = color;
            _id = player.CurrentBlipCounter++;

            player.TriggerEvent(PlayerEvents.BLIP_CREATE, _id, _position, _sprite, (int)_color);

            IsCreated = true;
        }

        public void SetColor(BlipColor color)
        {
            if (!IsCreated) return;
            _color = color;
            _player.TriggerEvent(PlayerEvents.BLIP_SET_COLOR, _id, (int)_color);
        }

        public void SetSprite(int sprite)
        {
            if (!IsCreated) return;
            _sprite = sprite;
            _player.TriggerEvent(PlayerEvents.BLIP_SET_COLOR, _id, sprite);
        }

        public void Dispose()
        {
            if (!IsCreated) return;
            _player.TriggerEvent(PlayerEvents.BLIP_DESTROY, _id);
            IsCreated = false;
        }
    }

    public enum BlipColor
    {
        Red = 1,
        LightGreen = 2,
        LightBlue = 3,
        White = 4,
        Yellow = 5,
        Purple = 7,
        Pink = 8,
        LightGrey = 39,
        Grey = 40,
        DarkGreen = 52,
        Magenta = 61,
        Orange = 64,
        Black = 85
    }
}