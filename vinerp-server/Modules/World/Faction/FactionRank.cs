﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VineRP
{
    public class FactionRank
    {
        public int Sqlid;
        public string Name;
        public bool CanInvitePlayers;

        public FactionRank(int sqlid, string name)
        {
            Sqlid = sqlid;
            Name = name;
        }

        public static FactionRank Find(int sqlid)
        {
            FactionRank result = null;
            foreach (var ranks in FactionRanks.All)
            {
                result = ranks.ranks.FirstOrDefault(r => r.Sqlid == sqlid);
            }
            return result;
        }
    }
}
