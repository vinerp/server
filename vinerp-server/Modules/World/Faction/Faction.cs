﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;

namespace VineRP
{
    public abstract class Faction
    {
        public static List<Faction> All = new List<Faction>();

        public static FactionPolice Police;
        public static FactionGarbage Garbage;
        public static FactionTrucking Trucking;
        public static FactionFishing Fishing;

        public EventHandler<FactionEvent> FactionEvent = null;

        public string Name;
        public TextLabel FactionLabel;
        public int ID = 0;
        public Vector3 JoinPosition;
        public bool RequiresInvite = false;
        public bool HasRanks;
        public bool EditableRanks;
        public FactionRanks Ranks = null;

        protected Faction()
        {
        }

        protected void CreateLabel(string title)
        {
            FactionLabel = API.shared.createTextLabel(title, JoinPosition, 10f, 1f, true, 0);
        }

        public abstract void DisposeVehicles(Player player = null);

        protected abstract void FactionEventHandler(object sender, FactionEvent e);

        protected static int Register(Faction faction)
        {
            if(!All.Contains(faction)) All.Add(faction);

            faction.FactionEvent += faction.FactionEventHandler;

            if(faction.HasRanks) faction.Ranks = new FactionRanks(All.Count);

            return All.Count;
        }

        public static Faction Find(int id)
        {
            return id == 0 ? null : All.FirstOrDefault(f => f.ID == id);
        }

        public static void InitJobs()
        {
            Log.Info("Loading factions/jobs...");
            Police = new FactionPolice();
            Garbage = new FactionGarbage();
            Trucking = new FactionTrucking();
            Fishing = new FactionFishing();
        }

        public static void CleanupPlayerVehicle(Player player)
        {
            if (player.CurrentJobId < 1) return;

            foreach (var faction in Faction.All)
                faction.DisposeVehicles(player);

            /*var faction = Faction.Find(player.CurrentJobId);
            faction.DisposeVehicles(player);*/
        }
    }

    public class FactionEvent
    {
        public Player Player;
        public string Params;
    }
}
