﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using GTANetworkServer;
using GTANetworkShared;
using VineRP.Modules.Util;

namespace VineRP
{
    public class FactionTrucking : Faction
    {
        /* This is a list of vehicle spawn locations (at the parking lot in front).
           Format: Position, Rotation
        */
        private readonly List<Tuple<Vector3, Vector3>> VehicleSpawnLocations = new List<Tuple<Vector3, Vector3>>()
        {
            new Tuple<Vector3, Vector3>(new Vector3(769.3862, -2944.538, 5.376638), new Vector3(-0.1741638, 0.01912704, 117.7083)),
            new Tuple<Vector3, Vector3>(new Vector3(769.1496, -2959.915, 5.37706), new Vector3(-0.07779391, -0.04360803, 118.4378)),
            new Tuple<Vector3, Vector3>(new Vector3(770.1006, -2966.803, 5.376384), new Vector3(-0.1475126, 0.02832045, 114.0475)),
            new Tuple<Vector3, Vector3>(new Vector3(769.5425, -2978.83, 5.375734), new Vector3(-0.1011107, 0.006314038, 117.7551))
        };

        /* This is a list of delivery payments based on their type.
           Format: Delivery Type, Minimum Payment, Maximum Payment
        */
        private static readonly List<Tuple<DeliveryType, int, int>> DeliveryPay = new List<Tuple<DeliveryType, int, int>>()
        {
            new Tuple<DeliveryType, int, int>(DeliveryType.Small, 100, 150),
            new Tuple<DeliveryType, int, int>(DeliveryType.Medium, 250, 320),
            new Tuple<DeliveryType, int, int>(DeliveryType.Large, 320, 450)
        };

        /* This is the delivery target location where all packages should be delivered to. */
        private static Tuple<Vector3, CylinderColShape> DELIVERY_TARGET_LOCATION = new Tuple<Vector3, CylinderColShape>(new Vector3(891.0729, -3016.876, 5.477441), null);

        /* This is a list of delivery locations all around the map based on their type. */
        private static readonly List<Tuple<DeliveryType, Vector3, CylinderColShape>> DeliveryLocations = new List<Tuple<DeliveryType, Vector3, CylinderColShape>>()
        {
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Small, new Vector3(487.2728, -1227.699, 28.98333), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Small, new Vector3(-532.7886, -891.7654, 24.14936), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Small, new Vector3(-517.5658, -976.1947, 23.11183), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Small, new Vector3(-517.4308, -292.6616, 34.79111), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Small, new Vector3(-499.0756, -67.17235, 39.2654), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Small, new Vector3(-248.8817, -228.7011, 36.09478), null),

            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Medium, new Vector3(366.7611, 332.5968, 103.055), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Medium, new Vector3(-22.62328, 213.2218, 106.124), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Medium, new Vector3(-619.1603, 279.3987, 81.1578), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Medium, new Vector3(-1786.056, -382.1655, 44.48606), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Medium, new Vector3(-1611.538, -821.6121, 9.634843), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Medium, new Vector3(-1314.964, -1255.951, 4.148939), null),

            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Large, new Vector3(2677.804, 1607.768, 24.07208), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Large, new Vector3(2528.559, 2628.104, 37.52062), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Large, new Vector3(1767.71, 3307.461, 40.73874), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Large, new Vector3(1411.351, 3619.942, 34.47264), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Large, new Vector3(1557.281, 3799.804, 33.83091), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Large, new Vector3(2444.943, 4055.614, 37.64022), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Large, new Vector3(2517.244, 4125.845, 38.1), null),
            new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryType.Large, new Vector3(2551.814, 4673.896, 33.58419), null)
        };

        public static bool CMD_test1(Player player, string ps)
        {
            // Test scenario
            player.HasTruckingPackage = true;
            int index = DeliveryLocations.FindIndex(l => l.Item1 == player.TruckingDeliveryType);
            var deliveryLocation = DeliveryLocations[index];
            player.TruckingLocationIndex = index;
            OnPlayerEnterDeliveryLocation(deliveryLocation?.Item3 ?? DeliveryLocations[0].Item3, player.Client.handle);
            player.SendDebugMessage("Test scenario executed.");
            return true;
        }   

        public FactionTrucking()
        {
            Name = "Trucking Company";
            JoinPosition = new Vector3(805.623, -2947.64, 6.020661);
            CreateLabel("Trucking Job\nUse /job to start");
            HasRanks = false;

            /* Create colshapes for every delivery location */
            for (int i = 0; i < DeliveryLocations.Count; i++)
            {
                var colshape = API.shared.createCylinderColShape(DeliveryLocations[i].Item2, 5f, 5f);
                colshape.setData("location_index", i);
                colshape.onEntityEnterColShape += OnPlayerEnterDeliveryLocation;
                DeliveryLocations[i] = new Tuple<DeliveryType, Vector3, CylinderColShape>(DeliveryLocations[i].Item1, DeliveryLocations[i].Item2, colshape);
            }
            /* The delivery target is also given a colshape */
            var deliveryTargetLocationColShape = API.shared.createCylinderColShape(DELIVERY_TARGET_LOCATION.Item1, 5f,
                5f);
            deliveryTargetLocationColShape.setData("delivery_target", true);
            deliveryTargetLocationColShape.onEntityEnterColShape += OnPlayerEnterDeliveryLocation;
            DELIVERY_TARGET_LOCATION = new Tuple<Vector3, CylinderColShape>(DELIVERY_TARGET_LOCATION.Item1, deliveryTargetLocationColShape);

            ID = Register(this);
        }

        /*
            This is the event that handles collision shapes for this faction.
            This includes both delivery locations.
        */
        public static void OnPlayerEnterDeliveryLocation(ColShape shape, NetHandle client)
        {
            /* Error checking to ensure that players can only trigger the colshape. */
            if (API.shared.getEntityType(client) != EntityType.Player) return;
            var player = Player.GetPlayerFromNetHandle(client);
            if (player == null)
            {
                throw new Exception("Unable to find the player instance from a nethandle @ OnPlayerEnterDeliveryLocation");
                return;
            }
            if (!player.InVehicle) return;

            if (player.TruckingJobVehicle == null ||
                player.GetVehicle().vehicle.handle != player.TruckingJobVehicle?.vehicle.handle) return;

            player.SendDebugMessage("Enter trucking faction collision shape.");

            /* Determine if this is the delivery target since the same event is used for both, the location and the
               target. */
            bool isDeliveryTarget = (shape.hasData("delivery_target")) ? shape.getData("delivery_target") : false;
            if (isDeliveryTarget)
            {
                if (!player.HasTruckingPackage) return;

                player.SendInfoMessage("You are currently unloading the package.");
                player.Client.freeze(true);

                var unloadingTimer = new VTimer(1000 * 4, false);
                unloadingTimer.AddPlayerRelationship(player);

                unloadingTimer.Elapsed += (sender, args) =>
                {
                    player.HasTruckingPackage = false;

                    player.CurrentTruckingPackageBlip?.Dispose();
                    player.CurrentTruckingPackageMarker?.Dispose();

                    player.TriggerEvent(PlayerEvents.FACTION_TRUCKING_FINISH);
                    player.SendSuccessMessage("You have delivered the package.");
                    player.Client.freeze(false);

                    player.CurrentJobId = 0;

                    /* Pay the player based on the delivery type.*/
                    var deliveryPay = DeliveryPay.FirstOrDefault(p => p.Item1 == player.TruckingDeliveryType);
                    if (deliveryPay == null)
                    {
                        player.SendErrorMessage("Internal error: Delivery pay instance was not found.");
                        return;
                    }
                    int paymentAmount = Random.GetInt(deliveryPay.Item2, deliveryPay.Item3 + 1);
                    player.Money += paymentAmount;
                    player.SendSuccessMessage($"You have been paid ${paymentAmount} for this delivery.");
                };

                unloadingTimer.Start();
                return;
            }
            player.SendInfoMessage("check 1");

            if (player.CurrentJobId != Faction.Trucking.ID) return;
            if (!player.IsDriver) return;
            player.SendInfoMessage("check 2");

            int locationIndex = shape.getData("location_index");
            if (player.TruckingLocationIndex != locationIndex) return;
            player.SendInfoMessage("check 3");

            player.Client.freeze(true);

            player.SendInfoMessage("You are currently loading the package.");

            var loadingTimer = new VTimer(4 * 1000, false);
            loadingTimer.AddPlayerRelationship(player);

            loadingTimer.Elapsed += (sender, args) =>
            {
                /* The player has picked up the package, set their waypoint back to the base. */
                player.Client.freeze(false);
                player.SendSuccessMessage("You have picked up the package.");
                player.HasTruckingPackage = true;

                player.SendInfoMessage("The location has been marked on your map.");
                player.SetWaypoint(DELIVERY_TARGET_LOCATION.Item1.X, DELIVERY_TARGET_LOCATION.Item1.Y);

                player.CurrentTruckingPackageBlip.Dispose();
                player.CurrentTruckingPackageMarker.Dispose();

                player.CurrentTruckingPackageBlip = new Blip(player,
                    DELIVERY_TARGET_LOCATION.Item1, 478);

                player.CurrentTruckingPackageMarker = new Marker(player,
                    DELIVERY_TARGET_LOCATION.Item1, Color.YellowGreen);

                //player.TriggerEvent("modules_faction_trucking_job_deliver_target", DELIVERY_TARGET_LOCATION.Item1.X, DELIVERY_TARGET_LOCATION.Item1.Y, DELIVERY_TARGET_LOCATION.Item1.Z);
            };

            loadingTimer.Start();           
        }

        protected override void FactionEventHandler(object sender, FactionEvent e)
        {
            string[] allowedDeliveryTypes = new[] {"small", "medium", "large"};
            if (!e.Params.sscanf("s", out object[] args))
            {
                e.Player.SendUsageMessage("/job [delivery type]");
                e.Player.SendInfoMessage("Delivery types are: small, medium, large");
                return;
            }
            var deliveryTypeParameter = args[0] as string;
            if (deliveryTypeParameter == null)
            {
                e.Player.SendErrorMessage("Internal error: delivery type parameter is null.");
                return;
            }
            if (!allowedDeliveryTypes.Contains(deliveryTypeParameter.ToLower()))
            {
                e.Player.SendErrorMessage("This delivery type does not exist.");
                return;
            }

            Faction.CleanupPlayerVehicle(e.Player);

            e.Player.CurrentJobId = ID;

            DeliveryType type;

            if (deliveryTypeParameter.ToLower() == "small")
                type = DeliveryType.Small;
            else if (deliveryTypeParameter.ToLower() == "medium")
                type = DeliveryType.Medium;
            else if (deliveryTypeParameter.ToLower() == "large")
                type = DeliveryType.Large;
            else
                type = DeliveryType.Small;

            VehicleHash vehicleToSpawn = VehicleHash.Burrito;

            if (type == DeliveryType.Small)
                vehicleToSpawn = VehicleHash.Burrito;
            else if (type == DeliveryType.Medium)
                vehicleToSpawn = VehicleHash.Benson;
            else if (type == DeliveryType.Large)
                vehicleToSpawn = VehicleHash.Packer;

            e.Player.HasTruckingPackage = false;
            e.Player.TruckingDeliveryType = type;

            /* Determine a random trucking vehicle spawn location and spawn a vehicle */
            int randomVehicleLocationIndex = Random.GetInt(0, VehicleSpawnLocations.Count);

            e.Player.TruckingJobVehicle = new Vehicle(vehicleToSpawn, Faction.Trucking.ID);
            e.Player.TruckingJobVehicle.Create(VehicleSpawnLocations[randomVehicleLocationIndex].Item1, VehicleSpawnLocations[randomVehicleLocationIndex].Item2);
            e.Player.PutInVehicle(e.Player.TruckingJobVehicle);
            API.shared.setVehicleMod(e.Player.TruckingJobVehicle.vehicle.handle, 48, 2);

            e.Player.TruckingJobVehicle.vehicle.modColor1 = new GTANetworkServer.Constant.Color(0, 0, 0, 255);
            e.Player.TruckingJobVehicle.vehicle.modColor2 = new GTANetworkServer.Constant.Color(0, 0, 0, 255);
            e.Player.TruckingJobVehicle.vehicle.livery = 1;

            /* Find all delivery locations of a specific type */
            var deliveryLocationsOfType = DeliveryLocations.FindAll(d => d.Item1 == type);
            if (deliveryLocationsOfType.Count < 1)
            {
                throw new Exception($"No delivery locations were found for type {type.ToString()}");
            }
            Log.Debug($"Found {deliveryLocationsOfType.Count} locations of type {type}");

            /* Determine a random delivery location index from the list of delivery locations of a specific type */
            var deliveryLocationRandomIndex = Random.GetInt(0, deliveryLocationsOfType.Count);
            Log.Debug($"The random delivery location index is {deliveryLocationRandomIndex}");

            e.Player.SendSuccessMessage("The location of the package is marked on your map.");
            e.Player.SendInfoMessage("Your job is to retrieve the package and bring it back.");

            e.Player.TruckingLocationIndex =
                DeliveryLocations.FindIndex(
                    l => l.Item1 == type && l.Item2 == deliveryLocationsOfType[deliveryLocationRandomIndex].Item2);

            Log.Debug($"The real trucking location index is {e.Player.TruckingLocationIndex}");

            if (e.Player.TruckingLocationIndex == -1)
            {
                e.Player.SendErrorMessage("Internal error: truckingLocationIndex was not found.");
                return;
            }

            /* TODO: Using the wrong variable, use all delivery locations */

            /*
            e.Player.SetWaypoint(deliveryLocationsOfType[deliveryLocationRandomIndex].Item2.X,
                deliveryLocationsOfType[deliveryLocationRandomIndex].Item2.Y);

            e.Player.TriggerEvent("modules_faction_trucking_job_start", deliveryLocationsOfType[deliveryLocationRandomIndex].Item2.X,
                deliveryLocationsOfType[deliveryLocationRandomIndex].Item2.Y,
                deliveryLocationsOfType[deliveryLocationRandomIndex].Item2.Z);*/

            e.Player.SetWaypoint(DeliveryLocations[e.Player.TruckingLocationIndex].Item2.X,
                DeliveryLocations[e.Player.TruckingLocationIndex].Item2.Y);

            e.Player.CurrentTruckingPackageBlip = new Blip(e.Player,
                DeliveryLocations[e.Player.TruckingLocationIndex].Item2, 479);

            e.Player.CurrentTruckingPackageMarker = new Marker(e.Player,
                DeliveryLocations[e.Player.TruckingLocationIndex].Item2, Color.YellowGreen);
        }

        public override void DisposeVehicles(Player player = null)
        {
            player?.TruckingJobVehicle?.Dispose(true);
        }
    }

    public enum DeliveryType
    {
        Small,
        Medium,
        Large
    }
}
