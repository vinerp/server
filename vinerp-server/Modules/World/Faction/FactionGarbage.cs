﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using GTANetworkServer;
using GTANetworkShared;

namespace VineRP
{
    public class FactionGarbage : Faction
    {
        public static readonly Vector3 VEHICLE_SPAWN_POSITION = new Vector3(1367.014, -2075.519, 51.99852);
        public static readonly Vector3 TRUCK_EMPTY_POSITION = new Vector3(1402.979, -2055.594, 51.71342);

        public const int MAX_GARBAGE_PER_TRUCK = 10;
        public const int GARBAGE_LOADING_TIME = 1;
        public const int GARBAGE_UNLOAD_TIME = 5;

        public const int PAY_PER_LOCATION = 20;
        public const int BASE_PAY = 50;

        public static readonly List<List<Tuple<Vector3, SphereColShape>>> GARBAGE_LOCATIONS = new List<List<Tuple<Vector3, SphereColShape>>>()
        {
            new List<Tuple<Vector3, SphereColShape>>()
            {
                new Tuple<Vector3, SphereColShape>(new Vector3(899.8536, -1733.334, 29.91739), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(822.1719, -1625.735, 30.62975), null),
                /*new Tuple<Vector3, SphereColShape>(new Vector3(775.4178, -1405.002, 26.31497), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(775.9764, -1320.885, 25.70621), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(864.1667, -1143.943, 23.61144), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(824.7835, -1060.474, 27.42029), null)*/
            },
            new List<Tuple<Vector3, SphereColShape>>()
            {
                new Tuple<Vector3, SphereColShape>(new Vector3(1445.906, -1686.298, 65.53743), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(1256.464, -1656.616, 46.0622), null),
                /*new Tuple<Vector3, SphereColShape>(new Vector3(1168.323, -1646.23, 36.48514), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(435.1988, -1457.601, 28.83689), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(867.7673, -1571.105, 30.12505), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(787.1466, -1849.497, 28.84431), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(741.6938, -2025.331, 28.88091), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(718.9557, -2255.42, 28.82083), null),
                new Tuple<Vector3, SphereColShape>(new Vector3(1043.705, -2425.101, 28.55396), null)*/
            }
        };

        public FactionGarbage()
        {
            RequiresInvite = false;
            JoinPosition = new Vector3(1382.565, -2079.832, 51.99852);
            CreateLabel("Garbage Job\nUse /job to start");
            API.shared.createTextLabel("You can empty the garbage truck here\nUse /unload", TRUCK_EMPTY_POSITION, 25f, 1f, true, 0);
            ID = Register(this);

            for (int i = 0; i < GARBAGE_LOCATIONS.Count; i++)
            {
                for (int t = 0; t < GARBAGE_LOCATIONS[i].Count; t++)
                {
                    var sphereColShape = API.shared.createSphereColShape(GARBAGE_LOCATIONS[i][t].Item1, 5f);
                    sphereColShape.setData("location_index", t);
                    sphereColShape.onEntityEnterColShape += OnPlayerEnterGarbageLocation;

                    GARBAGE_LOCATIONS[i][t] = new Tuple<Vector3, SphereColShape>(GARBAGE_LOCATIONS[i][t].Item1, sphereColShape);
                }
            }
        }

        public static bool CMD_unload(Player player, string ps)
        {
            if (player.CurrentJobId != Faction.Garbage.ID)
            {
                player.SendErrorMessage("You currently do not work as a garbage truck driver.");
                return true;
            }
            if (player.Client.position.DistanceTo(TRUCK_EMPTY_POSITION) > 5f)
            {
                player.SendErrorMessage("You are not in the garbage unloading spot.");
                return true;
            }
            if (!player.Client.isInVehicle)
            {
                player.SendErrorMessage("You are not in a vehicle.");
                return true;
            }
            if (player.GarbageJobVehicle == null)
            {
                player.SendErrorMessage("You do not have your own garbage truck.");
                return true;
            }
            if (player.GetVehicle()?.vehicle.handle != player.GarbageJobVehicle.vehicle.handle)
            {
                player.SendErrorMessage("You are not in your garbage truck.");
                return true;
            }
            if (((int)player.GarbageJobVehicle.Cache.Get("faction_garbage_current_garbage_count")) <= 0)
            {
                player.SendErrorMessage("Your garbage truck does not have any garbage.");
                return true;
            }
            player.Client.freeze(true);
            player.SendInfoMessage("You are currently unloading the garbage.");

            var timer = new Timer
            {
                Interval = GARBAGE_UNLOAD_TIME * 1000,
                AutoReset = false
            };

            timer.Elapsed += (sender, args) =>
            {
                player.Client.freeze(false);

                var currentGarbageCount = (int)player.GarbageJobVehicle.Cache.Get("faction_garbage_current_garbage_count");
                int earnedAmount = (currentGarbageCount * PAY_PER_LOCATION) + BASE_PAY;
                player.SendSuccessMessage($"You have earned ${earnedAmount} for emptying this truck.");
                player.Money += earnedAmount;

                player.GarbageJobVehicle.Cache.Set("faction_garbage_current_garbage_count", 0);
                player.SendSuccessMessage("You have unloaded the garbage.");
                /* Fake/reuse the player enter vehicle trigger to update the current garbage. */
                player.TriggerEvent("modules_faction_garbage_vehicle_enter", 0, MAX_GARBAGE_PER_TRUCK);
            };

            timer.Start();
            return true;
        }

        public override void DisposeVehicles(Player player = null)
        {
            if (player != null)
            {
                if(player.GarbageJobVehicle != null) player.GarbageJobVehicle.Dispose(true);
            }
        }

        protected override void FactionEventHandler(object sender, FactionEvent e)
        {
            /* TODO: Check if the user isn't in another job */
            if (e.Player.CurrentJobId > 0)
            {
                /* Dispose current vehicles for the old job */
                Faction.Find(e.Player.CurrentJobId).DisposeVehicles(e.Player);
            }

            if (e.Player.GarbageJobVehicle != null)
            {
                e.Player.GarbageJobVehicle.Dispose(true);
                e.Player.GarbageJobVehicle = null;
            }

            e.Player.GarbageJobVehicle = new Vehicle(VehicleHash.Trash, ID);
            e.Player.GarbageJobVehicle.Create(VEHICLE_SPAWN_POSITION, new Vector3(0, 0, 0), 0);
            e.Player.PutInVehicle(e.Player.GarbageJobVehicle);

            e.Player.CurrentJobId = ID;

            var random = new Random();
            if (e.Player.LastGarbageLocationSet == -1)
            {
                // if it's the first the player is doing the job, pick a random location set

                e.Player.GarbageLocationSet = Random.GetInt(0, GARBAGE_LOCATIONS.Count);
                e.Player.LastGarbageLocationSet = e.Player.GarbageLocationSet;
            }
            else
            {
                var generatedLocationSet = -1;
                do
                {
                    generatedLocationSet = Random.GetInt(0, GARBAGE_LOCATIONS.Count);
                } while (generatedLocationSet == e.Player.LastGarbageLocationSet);
                e.Player.GarbageLocationSet = generatedLocationSet;
            }

            e.Player.CurrentGarbageLocation = 0;

            e.Player.GarbageJobVehicle.Cache.Set("faction_garbage_current_garbage_count", 0);

            e.Player.TriggerEvent(PlayerEvents.FACTION_GARBAGE_START, 0, MAX_GARBAGE_PER_TRUCK);

            e.Player.CurrentGarbageBlip = new Blip(e.Player,
                GARBAGE_LOCATIONS[e.Player.GarbageLocationSet][e.Player.CurrentGarbageLocation].Item1, 318);

            e.Player.CurrentGarbageMarker = new Marker(e.Player,
                GARBAGE_LOCATIONS[e.Player.GarbageLocationSet][e.Player.CurrentGarbageLocation].Item1, Color.YellowGreen);

            e.Player.SetWaypoint(GARBAGE_LOCATIONS[e.Player.GarbageLocationSet][e.Player.CurrentGarbageLocation].Item1.X,
                    GARBAGE_LOCATIONS[e.Player.GarbageLocationSet][e.Player.CurrentGarbageLocation].Item1.Y);
        }

        public static void OnPlayerEnterGarbageLocation(ColShape shape, NetHandle client)
        {
            var player = Player.GetPlayerFromNetHandle(client);
            if (player == null) return;
            var locationIndex = shape.getData("location_index");
            if (player.CurrentJobId != Faction.Garbage.ID)
                return;
            Log.Debug($"Current garbage location {player.CurrentGarbageLocation} expected {locationIndex}");
            if (player.CurrentGarbageLocation != locationIndex)
                return;

            if (!player.IsDriver) return;

            if (((int)player.GetVehicle()?.Cache.Get("faction_garbage_current_garbage_count")) >=
                FactionGarbage.MAX_GARBAGE_PER_TRUCK)
            {
                player.SendInfoMessage("Your truck is full of garbage. You can empty the truck at the base.");
                player.SendInfoMessage("The base has been marked on your map with an orange circle.");
                player.TriggerEvent("modules_faction_garbage_set_base_blip", TRUCK_EMPTY_POSITION.X, TRUCK_EMPTY_POSITION.Y, TRUCK_EMPTY_POSITION.Z);
                return;
            }

            player.SendInfoMessage("checks");
            /* freeze player */
            player.Client.freeze(true);

            var timer = new Timer
            {
                Interval = 1000 * GARBAGE_LOADING_TIME,
                AutoReset = false
            };
            timer.Elapsed += (sender, args) =>
            {
                player.Client.freeze(false);
                if (player.CurrentGarbageLocation != GARBAGE_LOCATIONS[player.GarbageLocationSet].Count - 1) player.SendSuccessMessage("New garbage location has been set.");
            };
            timer.Start();

            player.SendInfoMessage("You are currently loading garbage.");

            player.GetVehicle()?.Cache.Set("faction_garbage_current_garbage_count", ((int)player.GetVehicle().Cache.Get("faction_garbage_current_garbage_count")) + 1);
            Log.Debug($"Updated vehicle garbage coutn cache with {player.GetVehicle()?.Cache.Get("faction_garbage_current_garbage_count") ?? "null"}");

            if (player.CurrentGarbageLocation == GARBAGE_LOCATIONS[player.GarbageLocationSet].Count - 1)
            {
                // player just finished the last location
                player.TriggerEvent(PlayerEvents.FACTION_GARBAGE_FINISH);

                player.SendSuccessMessage("You have finished collecting garbage in this location.");
                player.SendInfoMessage("Return back to base to start a new location.");
                player.SendInfoMessage("The base has been marked on your gps.");
                player.SendInfoMessage("To collect your pay you must unload the garbage truck at the base.");

                player.CurrentGarbageBlip.Dispose();
                player.CurrentGarbageMarker.Dispose();

                player.SetWaypoint(TRUCK_EMPTY_POSITION.X, TRUCK_EMPTY_POSITION.Y);
            }
            else
            {
                // Update the next marker from the location set
                player.CurrentGarbageLocation++;
                player.TriggerEvent(PlayerEvents.FACTION_GARBAGE_UPDATE,
                    player.GetVehicle()?.Cache.Get("faction_garbage_current_garbage_count") ?? -1);

                player.CurrentGarbageBlip.Dispose();
                player.CurrentGarbageMarker.Dispose();

                player.CurrentGarbageBlip = new Blip(player,
                    GARBAGE_LOCATIONS[player.GarbageLocationSet][player.CurrentGarbageLocation].Item1, 318);

                player.CurrentGarbageMarker = new Marker(player,
                    GARBAGE_LOCATIONS[player.GarbageLocationSet][player.CurrentGarbageLocation].Item1, Color.YellowGreen);

                player.SetWaypoint(GARBAGE_LOCATIONS[player.GarbageLocationSet][player.CurrentGarbageLocation].Item1.X,
                    GARBAGE_LOCATIONS[player.GarbageLocationSet][player.CurrentGarbageLocation].Item1.Y);
            }

            
        }
    }
}
