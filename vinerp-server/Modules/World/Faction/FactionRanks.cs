﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VineRP;

namespace VineRP
{
    public class FactionRanks
    {
        public static List<FactionRanks> All = new List<FactionRanks>();

        public const string Table = "factionranks";
        
        /*                sqlid, name */
        public List<FactionRank> ranks = new List<FactionRank>();
        public readonly int FactionId;

        public FactionRanks(int factionid)
        {
            FactionId = factionid;
            Load();
        }

        private void Load()
        {
            Log.Info($"Loading faction ranks for {Faction.All[FactionId-1].Name}");
            ranks.Clear();

            var query = new DBQuery($"SELECT * FROM `{Table}` WHERE factionId='{FactionId}' ORDER BY id ASC");
            query.Execute();

            if (query.GetResult().HasRows)
            {
                while (query.GetResult().Read())
                {
                    var rank = new FactionRank(query.GetResult().GetInt32("id"), query.GetResult().GetString("name"));

                    rank.CanInvitePlayers = (query.GetResult().GetInt32("canInvitePlayers") == 1) ? true : false;

                    ranks.Add(rank);
                }
            }

            query.Close();
        }
    }
}
