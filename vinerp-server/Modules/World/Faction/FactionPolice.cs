﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GTANetworkServer;
using GTANetworkShared;

namespace VineRP
{
    public class FactionPolice : Faction
    {
        public const string TableTickets = "tickets";

        /* The ranks for the LSPD faction.
           Format: name, can invite players
        */
        public static List<Tuple<string>> Ranks = new List<Tuple<string>>()
        {
            new Tuple<string>("Police Officer I"),
            new Tuple<string>("Police Officer II"),
            new Tuple<string>("Police Officer III"),
            new Tuple<string>("Police Officer III+"),
            new Tuple<string>("Detective I"),
            new Tuple<string>("Detective II"),
            new Tuple<string>("Detective III"),
            new Tuple<string>("Seargeant I"),
            new Tuple<string>("Seargeant II"),
            new Tuple<string>("Lieutenant I"),
            new Tuple<string>("Lieutenant II"),
            new Tuple<string>("Captain I"),
            new Tuple<string>("Captain II"),
            new Tuple<string>("Commander"),
            new Tuple<string>("Deputy Chief of Police"),
            new Tuple<string>("Assistant Chief of Police"),
            new Tuple<string>("Chief of Police"),
        };

        public static readonly Vector3 TicketPayPosition = new Vector3(440.8241, -981.1292, 30.68961);
        public static readonly Vector3 LockerRoomPosition = new Vector3(454.5568, -989.7511, 30.68961);
        public static readonly Vector3 ArmoryPosition = new Vector3(451.44, -980.2859, 30.68961);

        public static List<Tuple<int, int, string>> PlayerTickets = new List<Tuple<int, int, string>>();

        public const int PHONE_TRACE_DURATION = 2000;
        public const int PHONE_TRACE_COOLDOWN = 90 * 1000;

        public FactionPolice()
        {
            Name = "Los Santos Police Department";
            RequiresInvite = true;
            JoinPosition = new Vector3();
            CreateLabel($"{Name}\nTo join this faction use /job");
            API.shared.createTextLabel("You can pay your tickets here.\nUse /payticket", TicketPayPosition, 12f, 1f);

            API.shared.createTextLabel("Use /duty to start or stop your duty", LockerRoomPosition, 12f, 1f);

            HasRanks = true;
            EditableRanks = false;

            ID = Register(this);

            LoadTickets();
        }

        public static void LoadTickets()
        {
            var query = new DBQuery($"SELECT * FROM `{TableTickets}`", QueryType.QUERY);
            query.Execute();

            if (query.GetResult().HasRows)
            {
                while (query.GetResult().Read())
                {
                    PlayerTickets.Add(new Tuple<int, int, string>(
                        query.GetResult().GetInt32("player_id"),
                        query.GetResult().GetInt32("cost"),
                        query.GetResult().GetString("reason")
                    ));
                }
            }

            query.Close();
        }

        public static void SaveTickets()
        {
        }

        public static List<Tuple<int, int, string>> GetPlayerTickets(Player player)
        {
            var tickets = new List<Tuple<int, int, string>>();
            foreach (var ticket in PlayerTickets)
            {
                if(ticket.Item1 == player.Sqlid) tickets.Add(ticket);
            }
            return tickets;
            //return PlayerTickets.FindAll(t => t.Item1 == player.Sqlid);
        }

        protected override void FactionEventHandler(object sender, FactionEvent e)
        {
            if (!e.Player.FactionInvites.Contains(ID))
            {
                e.Player.SendErrorMessage("You are required to have an invite to join this faction.");
                return;
            }
        }

        public override void DisposeVehicles(Player player = null)
        {
            
        }
    }
}
