﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using GTANetworkServer;
using GTANetworkShared;

namespace VineRP
{
    /* Not an actual faction */
    public class FactionFishing
    {
        /* The minimum weight of the fish that the player could catch */
        public const float MIN_FISH_WEIGHT = 1f;
        /* The minimum weight of the fish that the player could catch */
        public const float MAX_FISH_WEIGHT = 15f;
        /* This is how much the player gets paid for every pound of fish they sell.*/
        public const int PAY_PER_FISH_POUND = 5;

        /* The minimum amount of time that the player will have to wait to potential catch a fish */
        public const int MIN_FISH_CATCH_DURATION = 7;
        /* The maximum amount of time that the player will have to wait to potential catch a fish */
        public const int MAX_FISH_CATCH_DURATION = 12;

        public static readonly Vector3 FISH_SELL_POSITION = new Vector3(443.4554, -3115.028, 6.070059);

        public static List<Vector3> fishingAreas = new List<Vector3>()
        {
            new Vector3(-1827.982, -1269.118, 8.618269),
            new Vector3(-1836.722, -1261.802, 8.615791),
            new Vector3(-1849.328, -1251.208, 8.615791),
            new Vector3(-1861.712, -1240.832, 8.615777),
            new Vector3(-1862.073, -1233.41, 8.615777),
            new Vector3(-1823.568, -1267.147, 8.618272),
            new Vector3(-1802.712, -1242.317, 8.615772),
            new Vector3(-1802.142, -1228.913, 1.613757)
        };

        public static readonly string[] fishNames = new[]
        {
            "Barbel", "Bleak", "Bream", "Carp", "Crucian Carp", "Chub",
            "Dace", "Eel", "Grayling", "Gudgeon", "Perch", "Pike",
            "Roach", "Ruffe", "Rudd", "Silver Bream", "Tench", "Wels Catfish",
            "Zander", "Herring", "Cod", "Salmon", "Mackarel"
        };

        public FactionFishing()
        {
            foreach (var fishingArea in fishingAreas)
            {
                API.shared.createTextLabel("Fishing area\nUse /fish\n", fishingArea, 15f, 1f, false);
            }

            API.shared.createTextLabel("~g~You can sell all of your fish here\nUse /fsellall", FISH_SELL_POSITION, 15f, 1f, false);
        }

        public static bool CMD_fpos(Player player, string ps)
        {
            player.Client.position = FISH_SELL_POSITION;
            return true;
        }

        public static bool CMD_fsellall(Player player, string ps)
        {
            var hasFish = Item.Fish.Any(fishItem => player.Inventory.HasItem(fishItem));

            if (!hasFish)
            {
                return player.SendErrorMessage("You do not have any fish to sell.");
            }

            var totalFishWeight = 0f;

            foreach (var fishItem in Item.Fish)
            {
                if (player.Inventory.HasItem(fishItem))
                {
                    totalFishWeight += player.Inventory.GetItemQuantity(fishItem);
                    player.Inventory.Take(fishItem, player.Inventory.GetItemQuantity(fishItem));
                }
            }

            var earnings = (int)System.Math.Round(totalFishWeight * PAY_PER_FISH_POUND, 0);
            
            player.Money += earnings;
            player.SendSuccessMessage($"You have sold {totalFishWeight}lbs. of fish for ${earnings}");
            return true;
        }

        public static bool CMD_fish(Player player, string ps)
        {
            if (!IsPlayerInFishingArea(player))
            {
                player.SendErrorMessage("You are not in a fishing area.");
                return true;
            }
            /*if (!player.Inventory.HasItem(Item.FishingRod))
            {
                player.SendErrorMessage("You do not have a fishing rod.");
                return true;
            }*/
            /* Using fish 0 since all fish items are the same */
            if (!player.Inventory.HasSpaceForItem(Item.Fish[0], MAX_FISH_WEIGHT))
            {
                player.SendErrorMessage("You do not have enough space inventory for a potential fish.");
                return true;
            }
            var timer = new VTimer(1000 * (Random.GetInt(MIN_FISH_CATCH_DURATION, MAX_FISH_CATCH_DURATION)), false);
            timer.AddPlayerRelationship(player);

            timer.Elapsed += (sender, args) =>
            {
                bool caughtFish = (Random.GetInt(0, 100) <= 35);

                if (caughtFish)
                {
                    int randomFishNameIndex = Random.GetInt(0, fishNames.Length);
                    float randomFishWeight = (float)System.Math.Round(Random.GetFloat(MIN_FISH_WEIGHT, MAX_FISH_WEIGHT), 2);
                    int fishValue = (int)System.Math.Round(randomFishWeight * PAY_PER_FISH_POUND, 0);

                    player.SendSuccessMessage($"You have caught a {fishNames[randomFishNameIndex]}. (Weight: {randomFishWeight}lbs. Value: ${fishValue})");
                    player.Inventory.Give(Item.Fish[randomFishNameIndex], randomFishWeight);
                }
                else
                {
                    player.SendInfoMessage("You were unsuccessful in catching a fish this time.");
                }

                player.Client.freeze(false);
            };

            timer.Start();

            player.SendInfoMessage("You have casted your fishing line.");
            player.Client.freeze(true);

            return true;
        }

        public static bool IsPlayerInFishingArea(Player player)
        {
            foreach (var fishingArea in fishingAreas)
            {
                if (player.Client.position.DistanceTo(fishingArea) <= 2.5f) return true;
            }
            return false;
        }

        public static bool DoesPlayerHaveFish(Player player)
        {
            foreach (var fish in Item.Fish)
            {
                if (player.Inventory.HasItem(fish)) return true;
            }
            return false;
        }
    }
}
