﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptSharp;
using GTANetworkServer;

namespace VineRP
{
    public class AuthenticationManager
    {
        public AuthenticationManager()
        {
            Log.Info("Starting authentication manager.");

            Main.Instance.API.onClientEventTrigger += (sender, name, args) =>
            {
                var player = Player.GetPlayerFromClient(sender);
                if (player == null)
                {
                    Log.Error("Got player as null @ AuthenticationManager.onClientEventTrigger");
                    return;
                }
                if (name.Equals(PlayerEvents.AUTH_LOGIN))
                {
                    if (player.IsLoggedIn) return;

                    var username = (string) args[0];
                    var password = (string) args[1];

                    Log.Debug($"Login attempt ({player.Name}) - {username}:{password}");

                    /* Attempt a UCP login */
                    var loginAttempt = AttemptUCPLogin(player, username, password);

                    /* If the login did not succeed, send an error to the user */
                    if (loginAttempt == UCPLoginReturnCode.UserDoesntExist
                        || loginAttempt == UCPLoginReturnCode.WrongPassword)
                    {
                        player.SendErrorMessage("The user does not exist or the password is wrong.");
                    }
                    else
                    {
                        /* Check if a player with this UCP user id is already in game */
                        var isPlayerAlreadyLoggedIn = false;
                        foreach (var op in Player.All)
                        {
                            if (op == player) continue;
                            if (op.UCPUserID == player.UCPUserID)
                            {
                                isPlayerAlreadyLoggedIn = true;
                                break;
                            }
                        }

                        if (isPlayerAlreadyLoggedIn)
                        {
                            player.Client.kick("This account is already logged in.");
                            return;
                        }

                        player.SendDebugMessage("Logged in successfully.");
                        player.EnableChat();

                        player.GetBrowserByName("main").Reset();
                        player.GetBrowserByName("main").Hide(true);

                        ShowCharacterMenu(player);
                    }
                }
            };
        }

        public static void ShowCharacterMenu(Player player)
        {
            var characterList = new List<Tuple<string, int, int>>();
            var characterMenu = new Menu(player, 0, 0, MenuAnchor.MiddleRight, "", "Choose your character:", false);

            var query = new DBQuery($"SELECT * FROM `{Player.Table}` WHERE user_id='{player.UCPUserID}'", QueryType.QUERY);
            query.Execute();

            if (query.GetResult().HasRows)
            {
                while (query.GetResult().Read())
                {
                    characterList.Add(new Tuple<string, int, int>(
                        query.GetResult().GetString("name"),
                        query.GetResult().GetInt32("money"),
                        query.GetResult().GetInt32("moneyBank")
                    ));
                }
            }

            query.Close();

            characterMenu.AddItem("Exit", "Exit the server");

            foreach (var character in characterList)
            {
                characterMenu.AddItem(character.Item1, $"Money: ${character.Item2} (${character.Item3} in bank)");
            }

            characterMenu.OnItemSelect += (o, margs) =>
            {
                if (margs.Index == 0)
                {
                    player.Client.kick();
                    return;
                }
                if (margs.Index > characterList.Count)
                {
                    player.Client.kick();
                    return;
                }
                player.Name = characterList[margs.Index - 1].Item1;
                player.IsLoggedIn = true;

                player.Load(characterList[margs.Index-1].Item1);
                player.LoadPersonalVehicles();
                player.OnLogin();

                player.TriggerEvent(PlayerEvents.AUTH_RESET_CAMERA);
                player.Client.freeze(false);
            };

            characterMenu.OnMenuClose += (o, eventArgs) =>
            {
                player.Client.kick();
            };

            characterMenu.Show();
            Log.Debug("Showing character menu");
        }

        public static bool Exists(Player player)
        {
            var query = new DBQuery($"SELECT * FROM `{Player.Table}` WHERE name=@Name").AddParam("@Name", player.Client.name);
            var retValue = query.Execute(true).HasRows;
            query.Close();
            return retValue;
        }

        public static bool AttemptLogin(Player player, string password)
        {
            Log.log("Attempting log in with sha " + Hash.SHA256Hash(password));
            var query = new DBQuery($"SELECT * FROM `{Player.Table}` WHERE name=@Name AND password=@Password LIMIT 1");
            query.AddParam("@Name", player.Client.name).AddParam("@Password", Hash.SHA256Hash(password)).Execute(true);
            var retValue = query.GetResult().HasRows;
            query.Close();
            return retValue;
        }

        public static UCPLoginReturnCode AttemptUCPLogin(Player player, string username, string password)
        {
            var query = new DBQuery($"SELECT * FROM `{Player.TableUCP}` WHERE username=@username LIMIT 1", QueryType.QUERY);
            query.AddParam("@username", username);
            query.Execute();
            if (query.GetResult().HasRows)
            {
                query.GetResult().Read();

                var passwordHash = query.GetResult().GetString("password");
                var userID = query.GetResult().GetInt32("id");
                query.Close();
                if (Crypter.CheckPassword(password, passwordHash))
                {
                    player.UCPUserID = userID;
                    return UCPLoginReturnCode.Success;
                }
                return UCPLoginReturnCode.WrongPassword;
            }
            query.Close();
            return UCPLoginReturnCode.UserDoesntExist;
        }

        public static void Register(Player player, string password)
        {
            DBQuery query = new DBQuery($"INSERT INTO `{Player.Table}` (name, password) VALUES(@Name, @Password)", QueryType.INSERT_OR_DELETE);
            query.AddParam("@Name", player.Client.name).AddParam("@Password", Hash.SHA256Hash(password)).Execute();
            // generate a random phone number
            player.PhoneNumber = Player.GeneratePhoneNumber();
        }
    }

    public enum UCPLoginReturnCode
    {
        UserDoesntExist = 0,
        WrongPassword = 1,
        Success = 2
    }
}
