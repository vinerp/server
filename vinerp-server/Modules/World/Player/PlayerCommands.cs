﻿using VineRP.Modules.Util;
using GTANetworkShared;
using System;
using System.Data.SqlClient;
using System.Linq;
using GTANetworkServer;
using Timer = System.Timers.Timer;

namespace VineRP
{
    sealed class PlayerCommands
    {
        public static bool CMD_animations(Player player, string ps)
        {
            player.SendCustomMessage("Animations", Color.DarkCyan.RGB(), "/walk /copidle /madtalk /tablesit /handsup");
            player.SendCustomMessage("Animations", Color.DarkCyan.RGB(), "Cancel animation: /cancelanim");
            return true;
        }

        public static bool CMD_walk(Player player, string ps)
        {
            player.Client.playAnimation("amb@code_human_wander_idles_cop@male@idle_a", "idle_a", (int)(AnimationFlags.AllowPlayerControl | AnimationFlags.Loop));
            return true;
        }

        public static bool CMD_copidle(Player player, string ps)
        {
            player.Client.playAnimation("amb@code_human_police_investigate@base", "base", (int)AnimationFlags.Loop);
            return true;
        }

        public static bool CMD_madtalk(Player player, string ps)
        {
            player.Client.playAnimation("special_ped@impotent_rage@convo_1@convo_1a", "	im_an_actor_0", (int)AnimationFlags.Loop);
            return true;
        }

        public static bool CMD_tablesit(Player player, string ps)
        {
            player.Client.playAnimation("misslester1aig_9", "idle_sitting_peda", (int)AnimationFlags.Loop);
            return true;
        }

        public static bool CMD_handsup(Player player, string ps)
        {
            player.Client.playAnimation("missminuteman_1ig_2", "handsup_base", (int)AnimationFlags.Loop);
            return true;
        }

        /*
         * This command turns on the phone.
         */
        public static bool CMD_phoneon(Player player, string ps)
        {
            if (player.PhoneStatus)
            {
                return player.SendErrorMessage("Your phone is already on.");
            }
            player.PhoneStatus = true;
            return player.SendSuccessMessage("You turn on your phone.");
        }

        /*
         * This command turns off the phone.
         */
        public static bool CMD_phoneoff(Player player, string ps)
        {
            if (!player.PhoneStatus)
            {
                return player.SendErrorMessage("Your phone is already off.");
            }
            player.PhoneStatus = false;
            return player.SendSuccessMessage("You turn off your phone.");
        }

        /*
         * This command invites a player to a faction.
         */
        public static bool CMD_finvite(Player player, string ps)
        {
            if (player.Faction == null)
            {
                player.SendErrorMessage(CommonMessages.NOT_IN_FACTION);
                return true;
            }
            if (!ps.sscanf("u", out object[] args))
            {
                player.SendUsageMessage("/finvite [player name or id]");
                return true;
            }
            var target = (Player) args[0];
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            if (target == player)
            {
                player.SendErrorMessage("You cannot invite yourself.");
                return true;
            }

            if (player.Faction.ID == Faction.Police.ID)
            {
                if (player.FactionRank < 15)
                {
                    player.SendErrorMessage("Your rank does not permit the use of this command.");
                    return true;
                }
            }
            else
            {
                return true;
            }

            if (target.FactionInvites.Contains(player.Faction.ID))
            {
                player.SendErrorMessage("The player already has an invitation for this faction.");
                return true;
            }

            target.FactionInvites.Add(player.Faction.ID);
            target.SendInfoMessage($"You have been invited to join {player.Faction.Name}");
            player.SendSuccessMessage($"You have invited {target.Name} to join {player.Faction.Name}");
            return true;
        }

        public static bool CMD_clothes(Player player, string ps)
        {
            if (!ps.sscanf("ii", out object[] args))
            {
                return player.SendUsageMessage("/clothes [component] [clothes]");
            }
            var component = (int) args[0];
            var clothes = (int) args[1];

            if (component < 0 || component > 11)
            {
                return player.SendErrorMessage("Wrong component, must be 0-11.");
            }

            player.Client.setSkin(PedHash.FreemodeMale01);
            player.Client.setClothes(component, clothes, 0);
            player.SendSuccessMessage("Set");
            return true;
        }

        public static bool CMD_freeze(Player player, string ps)
        {
            player.Client.freezePosition = true;
            return true;
        }

        public static bool CMD_customize(Player player, string ps)
        {
            var cameraLocation = player.Client.position.Add(new Vector3(2, 0, 0));
            player.TriggerEvent(PlayerEvents.CUSTOMIZATION_CAMERA, cameraLocation, player.Client.position);
            player.Client.rotation = new Vector3(0, 0, -90);
            player.Client.freezePosition = true;
            API.shared.playPlayerAnimation(player.Client, 1 << 0, "amb@world_human_stand_guard@male@idle_a", "idle_a");
            player.GetBrowserByName("customization").Show(true);
            return true;
        }

        public static bool CMD_endc(Player player, string ps)
        {
            Log.Debug("endc command");
            player.TriggerEvent(PlayerEvents.CUSTOMIZATION_CAMERA_END);
            Log.Debug("trigger");
            player.Client.freezePosition = false;
            Log.Debug("freeze");
            player.GetBrowserByName("customization").Hide(true);
            Log.Debug("browser");
            return true;
        }

        public static bool CMD_rotate(Player player, string ps)
        {
            if (!ps.sscanf("fff", out object[] args))
            {
                return player.SendUsageMessage("/rotate [x] [y] [z]");
            }
            var x = (float) args[0];
            var y = (float) args[1];
            var z = (float) args[2];
            //player.Client.freezePosition = true;
            player.Client.rotation = new Vector3(x, y, z);
            return true;
        }

        public static bool CMD_locker(Player player, string ps)
        {
            player.Client.position = FactionPolice.LockerRoomPosition;
            return true;
        }

        public static bool CMD_duty(Player player, string ps)
        {
            if (player.Faction == null || player.Faction.ID != Faction.Police.ID)
            {
                return player.SendErrorMessage("This command is only for LSPD employees.");
            }
            if (player.Client.position.DistanceTo(FactionPolice.LockerRoomPosition) > 3.5f)
            {
                return player.SendErrorMessage("You must be in the locker room to go on or leave your duty.");
            }

            player.IsOnLSPDDuty = !player.IsOnLSPDDuty;

            if (player.IsOnLSPDDuty)
            {
                player.Client.nametagColor = new GTANetworkServer.Constant.Color(Color.Blue.R, Color.Blue.G, Color.Blue.B);

                player.Client.giveWeapon(WeaponHash.Nightstick, 1, true, true);
                player.Client.giveWeapon(WeaponHash.Pistol, 100, false, true);
                player.Client.giveWeapon(WeaponHash.StunGun, 10, false, true);
                player.Client.giveWeapon(WeaponHash.Flashlight, 1, false, true);
                player.Client.armor = 100;

                player.SendSuccessMessage("You have started your duty.");

            }
            else
            {
                player.Client.resetNametagColor();

                player.Client.removeWeapon(WeaponHash.Nightstick);
                player.Client.removeWeapon(WeaponHash.Pistol);
                player.Client.removeWeapon(WeaponHash.StunGun);
                player.Client.removeWeapon(WeaponHash.Flashlight);
                player.Client.armor = 0;

                player.SendSuccessMessage("You have ended your duty.");
            }
            return true;
        }

        /*
         * This command runs a phone trace on a specific number.
         */
        public static bool CMD_ptrace(Player player, string ps)
        {
            if (player.Faction == null || player.Faction.ID != Faction.Police.ID)
            {
                return player.SendErrorMessage("This command is only for LSPD employees.");
            }
            if (!player.PTraceCoolDownExpired)
            {
                return player.SendErrorMessage($"You may only use this command every {FactionPolice.PHONE_TRACE_COOLDOWN} seconds.");
            }
            if (!ps.sscanf("i", out object[] args))
            {
                player.SendUsageMessage("/ptrace [phone number]");
                return true;
            }
            var targetNumber = (int) args[0];
            Player targetPlayer = Player.All.Where(target => target.IsLoggedIn)
                                            .FirstOrDefault(target => target.PhoneNumber == targetNumber);

            if (targetPlayer == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            if (targetPlayer == player)
            {
                player.SendErrorMessage("You cannot trace yourself.");
                return true;
            }

            var traceTimer = new VTimer(FactionPolice.PHONE_TRACE_DURATION, false);
            traceTimer.AddPlayerRelationship(player);

            traceTimer.Elapsed += (sender, eventArgs) =>
            {
                if (targetPlayer.IsConnected)
                {
                    if (targetPlayer.PhoneStatus)
                    {
                        player.PTraceBlip?.Dispose();

                        player.PTraceBlip = new Blip(player, targetPlayer.Client.position, 458, BlipColor.Orange);
                        player.SendSuccessMessage("The phone trace was successful. The target was marked on your map.");
                    }
                    else
                    {
                        player.SendInfoMessage("The target phone number is offline.");
                    }

                    player.PTraceCoolDownExpired = false;

                    var traceCooldown = new VTimer(FactionPolice.PHONE_TRACE_COOLDOWN, false);
                    traceCooldown.AddPlayerRelationship(player);

                    traceCooldown.Elapsed += (o, elapsedEventArgs) =>
                    {
                        player.PTraceCoolDownExpired = true;
                    };

                    traceCooldown.Start();
                }
            };

            traceTimer.Start();

            player.SendInfoMessage("You are running a phone trace...");
            return true;
        }

        /// <summary>
        /// This command displays the current player tickets.
        /// </summary>
        public static bool CMD_mytickets(Player player, string ps)
        {
            if (FactionPolice.GetPlayerTickets(player).Count == 0)
            {
                player.SendInfoMessage("You do not have any tickets.");
                return true;
            }
            var playerTickets = FactionPolice.GetPlayerTickets(player);
            for (int i = 0; i < playerTickets.Count; i++)
            {
                player.SendInfoMessage($"{i+1}. ${playerTickets[i].Item2} - {playerTickets[i].Item3}");
            }
            return true;
        }

        /// <summary>
        /// This command is used to pay a ticket.
        /// </summary>
        public static bool CMD_payticket(Player player, string ps)
        {
            if (!ps.sscanf("i", out object[] args))
            {
                return player.SendUsageMessage("/payticket [ticket id]");
            }
            if (FactionPolice.GetPlayerTickets(player).Count == 0)
            {
                return player.SendInfoMessage("You do not have any outstanding tickets.");
            }

            var ticketId = (int) args[0];
            var playerTickets = FactionPolice.GetPlayerTickets(player);

            if (ticketId < 1 || ticketId > playerTickets.Count)
            {
                return player.SendErrorMessage("Invalid ticket id.");
            }

            var targetTicket = playerTickets[ticketId - 1];

            if (targetTicket.Item2 > player.Money)
            {
                return player.SendErrorMessage("You do not have enough money to pay for the ticket.");
            }

            player.Money -= targetTicket.Item2;
            FactionPolice.PlayerTickets.Remove(targetTicket);

            return player.SendSuccessMessage($"Ticket #{ticketId} has been paid successfully.");
        }
        
        /// <summary>
        /// This command is used to write a ticket to a player.
        /// </summary>
        public static bool CMD_ticket(Player player, string ps)
        {
            if (player.Faction == null || player.Faction.ID != Faction.Police.ID)
            {
                player.SendErrorMessage("This command is only for LSPD employees.");
                return true;
            }
            if (!ps.sscanf("uis", out object[] args))
            {
                return player.SendUsageMessage("/ticket [player name or id] [cost] [reason]");
            }
            var target = (Player) args[0];
            var cost = (int) args[1];
            var reason = (string) args[2];

            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            if (target == player)
            {
                player.SendErrorMessage("You cannot write a ticket to yourself.");
                return true;
            }
            if (cost < 0)
            {
                player.SendErrorMessage("Ticket cost cannot be less than zero.");
                return true;
            }
            if (reason.Length > 50)
            {
                player.SendErrorMessage("The ticket reason cannot be longer than 50 characters.");
                return true;
            }

            FactionPolice.PlayerTickets.Add(new Tuple<int, int, string>(target.Sqlid, cost, reason));

            target.SendInfoMessage($"{player.Name} has written you a ${cost} ticket for: {reason}");
            player.SendSuccessMessage("Ticket was successfully written.");
            return true;
        }
        
        /// <summary>
        /// This command is used to cancel a faction invitation for a player.
        /// </summary>
        public static bool CMD_funinvite(Player player, string ps)
        {
            if (player.Faction == null)
            {
                player.SendErrorMessage(CommonMessages.NOT_IN_FACTION);
                return true;
            }
            if (!ps.sscanf("u", out object[] args))
            {
                player.SendUsageMessage("/uninvite [player name or id]");
                return true;
            }
            var target = (Player) args[0];
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            if (target == player)
            {
                player.SendErrorMessage("You cannot uninvite yourself.");
                return true;
            }

            if (player.Faction.ID == Faction.Police.ID)
            {
                if (player.FactionRank < 15)
                {
                    player.SendErrorMessage("Your rank does not permit the use of this command.");
                    return true;
                }
            }
            else
            {
                return true;
            }

            if (!target.FactionInvites.Contains(player.Faction.ID))
            {
                player.SendErrorMessage("The player was not invited to this faction.");
                return true;
            }

            target.FactionInvites.Remove(player.Faction.ID);
            target.SendInfoMessage($"You have been uninvited to join {player.Faction.Name}");
            return true;
        }

        /*
         * This is a command for faction members that is used to assign a rank to other faction members.
         */
        public static bool CMD_rank(Player player, string ps)
        {
            if (player.Faction == null)
            {
                player.SendErrorMessage(CommonMessages.NOT_IN_FACTION);
                return true;
            }
            if (!ps.sscanf("ui", out object[] args))
            {
                player.SendUsageMessage("/rank [player name or id] [rank id]");
                player.SendInfoMessage("Rank ID can be found using /ranks");
                return true;
            }

            var target = (Player) args[0];
            var rank = (int) args[1];

            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            if (rank < 0)
            {
                player.SendErrorMessage("The rank ID cannot be lower than 0.");
                return true;
            }
            if (target == player)
            {
                player.SendErrorMessage("You cannot assign a rank to yourself.");
                return true;
            }

            if (target.Faction.ID != player.Faction.ID)
            {
                player.SendErrorMessage("The player is not in the same faction as you.");
                return true;
            }

            if (player.Faction.ID == Faction.Police.ID)
            {
                if (player.FactionRank < 7)
                {
                    player.SendErrorMessage("Your rank is not permitted to use this command.");
                    return true;
                }
                if (rank >= FactionPolice.Ranks.Count)
                {
                    player.SendErrorMessage("Invalid rank ID.");
                    return true;
                }
                target.FactionRank = rank;
                player.SendSuccessMessage("Rank was assigned successfully.");
                target.SendInfoMessage($"Your rank was changed to {FactionPolice.Ranks[rank]}");
            }
            return true;
        }

        /*
         * This command prints out all faction ranks.
         */
        public static bool CMD_ranks(Player player, string ps)
        {
            if (player.Faction == null)
            {
                player.SendErrorMessage(CommonMessages.NOT_IN_FACTION);
                return true;
            }
            if (player.Faction.ID == Faction.Police.ID)
            {
                for (int r = 0; r < FactionPolice.Ranks.Count; r++)
                {
                    player.SendCustomMessage("Rank", Color.Coral.RGB(), $"{FactionPolice.Ranks[r]} ID: {r}");
                }
                player.SendInfoMessage("Use PGUP and PGDN to scroll the chat.");
            }
            return true;
        }

        public static bool CMD_skin(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/skin [skin name]");
                return true;
            }
            var skinName = (string)args[0];
            if (!PedHash.TryParse(skinName, true, out PedHash skin))
            {
                player.SendErrorMessage("Skin was not found.");
                return true;
            }
            player.Client.setSkin(skin);
            player.SendSuccessMessage("Skin set successfully.");
            return true;
        }

        public static bool CMD_getdoor(Player player, string ps)
        {
            player.TriggerEvent("doormanager_debug");
            return true;
        }

        public static bool CMD_getobj(Player player, string ps)
        {
            player.TriggerEvent("select_object");
            return true;
        }

        public static bool CMD_dimension(Player player, string ps)
        {
            player.SendDebugMessage($"Your current dimension is {player.Client.dimension}");
            return true;
        }

        public static bool CMD_hospital(Player player, string ps)
        {
            player.Client.position = new Vector3(244.9f, -1374.7f, 39.5f);
            player.SendDebugMessage("teleported.");
            return true;
        }

        /// <summary>
        /// This command is used to lock or unlock a vehicle.
        /// </summary>
        public static bool CMD_vlock(Player player, string ps)
        {
            if (!player.InVehicle)
            {
                player.SendErrorMessage("You are not in a vehicle.");
                return true;
            }
            var vehicle = player.GetVehicle();
            if (!vehicle.OwnerName.Equals(player.Name))
            {
                player.SendErrorMessage("You are not the owner of this vehicle.");
                return true;
            }
            vehicle.Locked = !vehicle.Locked;
            if (vehicle.Locked)
            {
                player.SendCustomMessage("Vehicle", Color.White.RGB(), "~g~You have ~r~locked ~g~this vehicle.");
            }
            else
            {
                player.SendCustomMessage("Vehicle", Color.White.RGB(), "~g~You have ~w~unlocked ~g~this ~#FF0000~vehicle.");
            }
            return true;
        }

        /// <summary>
        /// This command is used to see players identification.
        /// </summary>
        public static bool CMD_showid(Player player, string ps)
        {
            if (!ps.sscanf("u", out object[] args))
            {
                player.SendUsageMessage("/showid [player name or id]");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage("Player was not found.");
                return true;
            }
            player.SendCustomMessage("ID", Color.LightGray.RGB(), $"Name: {target.Name}");
            player.SendCustomMessage("ID", Color.LightGray.RGB(), $"Gender: {target.Gender.ToString()}");
            player.SendCustomMessage("ID", Color.LightGray.RGB(), "Licenses: TODO");
            return true;
        }
        
        public static bool CMD_stats(Player player, string ps)
        {
            player.SendCustomMessage("Stats", Color.Yellow.RGB(), "Your statistics:");
            player.SendCustomMessage("Stats", Color.Yellow.RGB(), $"SSN: {player.SocialSecurityNumber}, Phone #: {player.PhoneNumber}");
            player.SendCustomMessage("Stats", Color.Yellow.RGB(),
                $"Minutes played: {player.MinutesPlayed} ({System.Math.Round(((double)(player.MinutesPlayed/60f)), 2)} hours)");
            player.SendCustomMessage("Stats", Color.Yellow.RGB(), $"Level: {player.Level} ({player.GetRequiredRankupHours()} hours required to level up)");
            player.SendCustomMessage("Stats", Color.Yellow.RGB(), $"Money: ${player.Money} (${player.BankMoney} in the bank)");
            return true;
        }

        public static bool CMD_invincible(Player player, string ps)
        {
            if (player.Group != PlayerGroup.Admin)
            {
                player.SendCommandPermissionDenied();
                return true;
            }
            player.Client.invincible = !player.Client.invincible;

            if(player.Client.invincible)
                player.SendSuccessMessage("Invincibility enabled.");
            else
                player.SendSuccessMessage("Invincibility disabled.");
            return true;
        }

        public static bool CMD_sethour(Player player, string ps)
        {
            if (!ps.sscanf("i", out object[] args))
            {
                player.SendUsageMessage("/sethour [hour]");
                return true;
            }
            var hour = Math.Clamp((int) args[0], 6, 23);
            API.shared.setTime(hour, 00);
            player.SendSuccessMessage("Time set successfully");
            return true;
        }

        public static bool CMD_anim(Player player, string ps)
        {
            if (!ps.sscanf("ss", out object[] args))
            {
                player.SendUsageMessage("/anim [dictionary] [name]");
                return true;
            }
            var animDict = (string) args[0];
            var animName = (string) args[1];

            API.shared.playPlayerAnimation(player.Client, 1 << 1, animDict, animName);
            player.SendSuccessMessage("Playing the animation.");
            return true;
        }

        public static bool CMD_cancelanim(Player player, string ps)
        {
            API.shared.stopPlayerAnimation(player.Client);
            player.SendSuccessMessage("Animation cancelled.");
            return true;
        }

        public static bool CMD_showb(Player player, string ps)
        {
            player.GetBrowserByName("main")?.Show(true);
            player.SendDebugMessage("done");
            return true;
        }

        public static bool CMD_hideb(Player player, string ps)
        {
            player.GetBrowserByName("main")?.Hide(true);
            player.SendDebugMessage("done");
            return true;
        }

        public static bool CMD_sms(Player player, string ps)
        {
            /* TODO: Check if the player has enough phone credit */
            if (!ps.sscanf("us", out object[] args))
            {
                player.SendUsageMessage("/sms [player name or id] [message]");
                return true;
            }
            return true;
        }

        public static bool CMD_rod(Player player, string ps)
        {
            if (player.Inventory.HasItem(Item.FishingRod))
            {
                player.SendDebugMessage("You have a fishing rod.");
            }
            else
            {
                player.SendDebugMessage("You do not have a fishing rod.");
            }
            return true;
        }

        public static bool CMD_heal(Player player, string ps)
        {
            if (player.Group != PlayerGroup.Admin)
            {
                player.SendCommandPermissionDenied();
                return true;
            }
            if (!ps.sscanf("u", out object[] args))
            {
                player.SendUsageMessage("/heal [player name or id]");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            target.Client.health = 100;
            player.SendSuccessMessage("Player has been healed successfully.");
            return true;
        }

        public static bool CMD_tpto(Player player, string ps)
        {
            if (player.Group != PlayerGroup.Admin)
            {
                player.SendCommandPermissionDenied();
                return true;
            }
            if (!ps.sscanf("u", out object[] args))
            {
                player.SendUsageMessage("/tpto [player name or id]");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }

            player.Client.position = target.Client.position;
            player.SendSuccessMessage("Teleported successfully.");
            return true;
        }

        public static bool CMD_tpget(Player player, string ps)
        {
            if(player.Group != PlayerGroup.Admin)
            {
                player.SendCommandPermissionDenied();
                return true;
            }
            if (!ps.sscanf("u", out object[] args))
            {
                player.SendUsageMessage("/tpget [player name or id]");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            target.Client.position = player.Client.position;
            player.SendSuccessMessage("Player was teleported to you successfully.");
            target.SendInfoMessage($"Administrator {player.Name} has teleported you.");
            return true;
        }

        public static bool CMD_auth(Player player, string ps)
        {
            player.GetBrowserByName("main").SetURL(Player.BASE_BROWSER_PATH + "auth/login.html", true, true);
            return true;
        }

        public static bool CMD_authdestroy(Player player, string ps)
        {
            player.GetBrowserByName("main").Hide(true);
            return true;
        }

        public static bool CMD_randomatm(Player player, string ps)
        {
            Random random = new Random();
            var locID = Random.GetInt(0, EconomyManager.ATMLocations.Count);
            player.Client.position = EconomyManager.ATMLocations[locID];
            player.SendSuccessMessage("Teleported...");
            return true;
        }

        public static bool CMD_save(Player player, string ps)
        {
            if (player.Group != PlayerGroup.Admin)
            {
                player.SendCommandPermissionDenied();
                return true;
            }
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/save [name]");
                return true;
            }
            var name = (string)args[0];
            var position = player.Client.position;
            var rotation = player.Client.rotation;
            if (player.Client.isInVehicle)
            {
                position = player.Client.vehicle.position;
                rotation = player.Client.vehicle.rotation;
            } 
            new DBQuery($"INSERT INTO `saves` (name, position, rotation) VALUES ('{name}', '{position.X}, {position.Y}, {position.Z}', '{rotation.X}, {rotation.Y}, {rotation.Z}')", QueryType.INSERT_OR_DELETE).Execute();
            player.SendSuccessMessage("Position saved.");
            return true;
        }

        /// <summary>
        /// This command is used to open or close the hood.
        /// </summary>
        public static bool CMD_hood(Player player, string ps)
        {
            if (!player.InVehicle)
            {
                return player.SendErrorMessage(CommonMessages.NOT_IN_VEHICLE);
            }

            var vehicle = player.GetVehicle();
            if (!vehicle.IsPlayerAuthorised(player))
            {
                return player.SendErrorMessage(CommonMessages.NOT_AUTHORISED);
            }

            vehicle.SetDoorState(VehicleDoor.Hood, !vehicle.GetDoorState(VehicleDoor.Hood));

            Player.SendRangedMessage(Color.HotPink.RGB(),
                $"* {player.Name} {((vehicle.GetDoorState(VehicleDoor.Hood)) ? "opens" : "closes")} the hood.", player.Client.position, 6f);
            return true;
        }

        /// <summary>
        /// This command is used to open or close the trunk
        /// </summary>
        public static bool CMD_trunk(Player player, string ps)
        {
            if (!player.InVehicle)
            {
                return player.SendErrorMessage(CommonMessages.NOT_IN_VEHICLE);
            }

            var vehicle = player.GetVehicle();
            if (!vehicle.IsPlayerAuthorised(player))
            {
                return player.SendErrorMessage(CommonMessages.NOT_AUTHORISED);
            }
            if (vehicle.Locked)
            {
                return player.SendErrorMessage("Cannot open the trunk whilst the car is locked.");
            }

            vehicle.SetDoorState(VehicleDoor.Trunk, !vehicle.GetDoorState(VehicleDoor.Trunk));

            Player.SendRangedMessage(Color.HotPink.RGB(),
                $"* {player.Name} {((vehicle.GetDoorState(VehicleDoor.Trunk)) ? "opens" : "closes")} the trunk.", player.Client.position, 6f);
            return true;
        }

        public static bool CMD_windows(Player player, string ps)
        {
            if (!player.Client.isInVehicle)
            {
                player.SendErrorMessage("You must be in a vehicle to control windows.");
                return true;
            }
            var vehicle = Vehicle.GetVehicleFromNetHandle(player.Client.vehicle.handle);
            vehicle.WindowsDown = !vehicle.WindowsDown;

            Player.SendRangedMessage(Color.HotPink.RGB(), $"* {player.Client.name} rolls {((vehicle.WindowsDown) ? "down" : "up")} the windows.", player.Client.position, 5f);
            return true;
        }

        public static bool CMD_job(Player player, string ps)
        {
            var nearestJob = Faction.All.FirstOrDefault(j => j.JoinPosition.DistanceTo(player.Client.position) <= 2.5f);
            if (nearestJob == null)
            {
                player.SendErrorMessage("You must be near a job to use this command.");
                return true;
            }
            var e = new FactionEvent()
            {
                Player = player,
                Params = ps
            };
            nearestJob.FactionEvent?.Invoke(null, e);
            return true;
        }

        public static bool CMD_pm(Player player, string ps)
        {
            if (!ps.sscanf("us", out object[] args))
            {
                player.SendUsageMessage("/pm [player name or id] [message]");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            player.SendCustomMessage("PM", Color.AliceBlue.RGB(), $"{player.Client.name}: {args[1]}");
            target.SendCustomMessage("PM", Color.AliceBlue.RGB(), $"{player.Client.name}: {args[1]}");
            return true;
        }

        public static bool CMD_id(Player player, string ps)
        {
            if (!ps.sscanf("u", out object[] args))
            {
                player.SendUsageMessage("/id [player name]");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            player.SendInfoMessage($"The unique ID for player {target.Client.name} is {target.Sqlid}");
            return true;
        }

        /* This is a global OOC chat command*/
        public static bool CMD_o(Player player, string ps)
        {
            if (player.Group != PlayerGroup.Admin)
            {
                player.SendCommandPermissionDenied();
                return true;
            }
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/o [message]");
                return true;
            }
            Player.SendMessageToAll($"{Color.LightGray.RGB()}[OOC] {player.Client.name} says: {args[0]}");
            return true;
        }

        public static bool CMD_kick(Player player, string ps)
        {
            if (player.Group != PlayerGroup.Admin)
            {
                player.SendCommandPermissionDenied();
                return true;
            }
            if (!ps.sscanf("us", out object[] args))
            {
                player.SendUsageMessage("/kick [player name or id] [reason]");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            var reason = (string) args[1];
            target.SendCustomMessage("Kick", Color.Yellow.RGB(), $"Administrator '{player.Name}' kicked you for '{reason}'.");
            player.SendSuccessMessage($"You have successfully kicked '{target.Name}' for '{reason}'");
            target.Client.kick();
            return true;
        }

        public static bool CMD_ban(Player player, string ps)
        {
            if (player.Group != PlayerGroup.Admin)
            {
                player.SendCommandPermissionDenied();
                return true;
            }
            if (!ps.sscanf("uiss", out object[] args))
            {
                player.SendUsageMessage("/ban [player name or id] [duration] [duration type] [reason]");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            var duration = (int) args[1];
            var durationType = (string) args[2];
            var reason = (string) args[3];
            if (reason.Length > 50)
            {
                player.SendErrorMessage("The reason must not be longer than 50 characters.");
                return true;
            }
            if(duration < 1)
            {
                player.SendErrorMessage("Duration cannot be less than 1.");
                return true;
            }
            if (durationType != "year" || durationType != "years" || durationType != "month" ||
                durationType != "months" || durationType != "day" || durationType != "days" || durationType != "hour" ||
                durationType != "hours" || durationType != "minute" || durationType != "minutes")
            {
                player.SendErrorMessage("Incorrect duration type. Allowed types are:");
                player.SendInfoMessage("year/s, month/s, day/s, hour/s, minute/s");
                return true;
            }
            var banTime = DateTime.UtcNow;
            if (durationType == "year" || durationType == "years")
                banTime = banTime.AddYears(duration);
            else if (durationType == "month" || durationType == "months")
                banTime = banTime.AddMonths(duration);
            else if (durationType == "day" || durationType == "days")
                banTime = banTime.AddDays(duration);
            else if (durationType == "hour" || durationType == "hours")
                banTime = banTime.AddHours(duration);
            else if (durationType == "minute" || durationType == "minutes")
                banTime = banTime.AddMinutes(duration);

            target.SendCustomMessage("Ban", Color.OrangeRed.RGB(), $"You have been banned until '{banTime:f}' for '{reason}'.");
            player.SendSuccessMessage($"Player '{target.Name}' was sucessfully banned until '{banTime:f}' for '{reason}'.");

            var query = new DBQuery($"INSERT INTO `banlist` (name, bannedBy, bannedUntil, reason) VALUES"+
                                    $" ('{target.Name}', '{player.Name}', '{Time.DateTimeToUnixTimestamp(banTime)}', '{reason}')", QueryType.INSERT_OR_DELETE);
            query.Execute();

            target.Client.kick();
            return true;
        }

        public static bool CMD_b(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/b [message]");
                return true;
            }
            Player.SendRangedMessage((new Color(145, 145, 145)).RGB(), $"(( {player.Client.name} says: {args[0]} ))", player.Client.position, 5f);
            return true;
        }

        public static bool CMD_vlist(Player player, string ps)
        {
            if (player.PersonalVehicles.Count < 1)
            {
                player.SendErrorMessage("You don't have any vehicles!");
                return true;
            }
            for (int i = 0; i < player.PersonalVehicles.Count; i++)
            {
                player.SendCustomMessage("Vehicle", Color.Aqua.RGB(), $"ID: {i} Name: {player.PersonalVehicles[i].DisplayName}");
            }
            return true;
        }

        public static bool CMD_v(Player player, string ps)
        {
            player.SendCustomMessage("Vehicle", Color.Azure.RGB(), "Commands: /vlist /vfind");
            return true;
        }

        /*public static bool CMD_finvite(Player player, string ps)
        {
            if (player.OwnedFaction == null)
            {
                player.SendErrorMessage("You are not an owner of a faction");
                return true;
            }
            if (!ps.sscanf("u", out object[] args))
            {
                player.SendUsageMessage("/finvite [player name or id]");
                return true;
            }
            var factionRank = FactionRank.Find(player.FactionRank);
            if (factionRank == null)
            {
                player.SendErrorMessage("Error occured trying to get your rank.");
                Log.Error($"Unable to find players' {player.Client.name} rank. Given id {player.FactionRank}");
                return true;
            }
            if (!factionRank.CanInvitePlayers)
            {
                player.SendErrorMessage("Your current rank does not allow inviting players into the faction.");
                return true;
            }
            var target = args[0] as Player;
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            if (target.FactionInvites.Contains(player.OwnedFaction.ID))
            {
                player.SendErrorMessage("This player was already invited to the faction");
                return true;
            }
            target.FactionInvites.Add(player.OwnedFaction.ID);
            player.SendSuccessMessage($"Player {target.Client.name} was invited to the faction.");
            target.SendInfoMessage($"You have been invited to join {player.OwnedFaction.Name}");
            return true;
        }*/

        public static bool CMD_vget(Player player, string ps)
        {
            if (player.PersonalVehicles.Count < 1)
            {
                player.SendErrorMessage("You don't have any vehicles");
                return true;
            }
            var spawnedInVehicle = player.PersonalVehicles.FirstOrDefault(v => v.IsCreated());
            if (spawnedInVehicle != null)
            {
                player.SendErrorMessage("You already have a spawned in vehicle.");
                return true;
            }
            // TODO: vget
            return true;
        }

        public static bool CMD_vfind(Player player, string ps)
        {
            if (player.PersonalVehicles.Count < 1)
            {
                player.SendErrorMessage("You don't have any vehicles!");
                return true;
            }
            Vehicle spawnedInVehicle = player.PersonalVehicles.FirstOrDefault(v => v.IsCreated());
            if (spawnedInVehicle == null)
            {
                player.SendErrorMessage("You do not have any spawned in vehicles.");
                return true;
            }
            player.TriggerEvent("modules_vehicle_personalvehicle_blip_show", spawnedInVehicle.Position);
            player.SendSuccessMessage("The vehicle was marked on your map.");
            return true;
        }

        public static bool CMD_ipl(Player player, string ps)
        {
            player.Client.position = new Vector3(-786.8663, 315.7642, 217.6385);
            player.SendSuccessMessage("Teleported");
            return true;
        }

        public static bool CMD_me(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/me [action]");
                return true;
            }
            Player.SendRangedMessage(Color.HotPink.RGB(), $"* {player.Client.name} {(string)args[0]}", player.Client.position, Player.BASE_TALK_RANGE);
            return true;
        }

        public static bool CMD_melow(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/melow [action]");
                return true;
            }
            Player.SendRangedMessage(Color.HotPink.RGB(), $"* {player.Client.name} {(string)args[0]}", player.Client.position, Player.BASE_TALK_RANGE * 0.60f);
            return true;
        }

        public static bool CMD_my(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/my [action]");
                return true;
            }
            Player.SendRangedMessage(Color.HotPink.RGB(), $"* {player.Client.name}'s {(string)args[0]}", player.Client.position, Player.BASE_TALK_RANGE);
            return true;
        }

        public static bool CMD_ame(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/ame [action]");
                return true;
            }
            if(player.AmeLabel != null) API.shared.deleteEntity(player.AmeLabel);
            player.AmeLabel = API.shared.createTextLabel((string) args[0], player.Client.position, Player.BASE_TALK_RANGE, 1f, false, 0);
            API.shared.setTextLabelColor(player.AmeLabel, 255, 105, 180, 255);
            API.shared.attachEntityToEntity(player.AmeLabel, player.Client.handle, "SKEL_Head", new Vector3(0, 0, 0.9f), new Vector3());
            Timer timer = new Timer(8000);
            timer.Elapsed += (sender, eventArgs) =>
            {
                if(player.AmeLabel != null) API.shared.deleteEntity(player.AmeLabel);
            };
            timer.AutoReset = false;
            timer.Start();
            return true;
        }

        public static bool CMD_amy(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/amy [action]");
                return true;
            }
            if (player.AmeLabel != null) API.shared.deleteEntity(player.AmeLabel);
            player.AmeLabel = API.shared.createTextLabel($"{player.Client.name}'s {(string)args[0]}", player.Client.position, Player.BASE_TALK_RANGE, 1f, false, 0);
            API.shared.setTextLabelColor(player.AmeLabel, 255, 105, 180, 255);
            API.shared.attachEntityToEntity(player.AmeLabel, player.Client.handle, "SKEL_Head", new Vector3(0, 0, 0.9f), new Vector3());
            Timer timer = new Timer(8000);
            timer.Elapsed += (sender, eventArgs) =>
            {
                if (player.AmeLabel != null) API.shared.deleteEntity(player.AmeLabel);
            };
            timer.AutoReset = false;
            timer.Start();
            return true;
        }

        public static bool CMD_do(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/do [action]");
                return true;
            }
            Player.SendRangedMessage(Color.HotPink.RGB(), $"* {(string)args[0]} (( {player.Client.name} ))", player.Client.position, Player.BASE_TALK_RANGE);
            return true;
        }

        public static bool CMD_dolow(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/dolow [action]");
                return true;
            }
            Player.SendRangedMessage(Color.HotPink.RGB(), $"* {(string)args[0]} (( {player.Client.name} ))", player.Client.position, Player.BASE_TALK_RANGE * 0.60f);
            return true;
        }

        public static bool CMD_s(Player player, string ps)
        {
            return CMD_shout(player, ps);
        }

        public static bool CMD_shout(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendUsageMessage("/shout [message]");
                return true;
            }
            var message = (string) args[0];
            if (message.Length > 100)
            {
                player.SendErrorMessage("Message cannot have more than 100 characters.");
                return true;
            }
            Player.SendRangedMessage(Color.Yellow.RGB(), $"{player.Name} shouts: {message}", player.Client.position, Player.BASE_TALK_RANGE * 2f);
            return true;
        }

        public static bool CMD_engine(Player player, string ps)
        {
            if (player.Client.isInVehicle)
            {
                var vehicle = player.GetVehicle();
                if (vehicle != null)
                {
                    if (player.DealershipShowroomVehicle != null && player.DealershipShowroomVehicle.vehicle.handle == vehicle.vehicle.handle)
                    {
                        player.SendErrorMessage("You cannot turn on the engine of a showroom vehicle.");
                        return true;
                    }
                    if (player.VehicleSeat != -1)
                    {
                        return player.SendErrorMessage("You must be the driver to use this command.");
                    }
                    if (!vehicle.IsPlayerAuthorised(player))
                    {
                        return player.SendErrorMessage("You don't have permission to do this.");
                    }
                    vehicle.EngineStatus = !vehicle.EngineStatus;
                    if (vehicle.EngineStatus)
                    {
                        Player.SendRangedMessage(Color.HotPink.RGB(), $"* {player.Client.name} has turned the vehicle's engine on.", player.Client.position, 5f);
                    }
                    else
                    {
                        Player.SendRangedMessage(Color.HotPink.RGB(), $"* {player.Client.name} has turned the vehicle's engine off.", player.Client.position, 5f);
                    }
                }
            }
            else
            {
                player.SendErrorMessage("You are not in a vehicle!");
            }
            return true;
        }

        public static bool CMD_myseat(Player player, string ps)
        {
            player.SendCustomMessage("DEBUG", Color.Aqua.RGB(), $"Your current seat is {player.VehicleSeat}");
            return true;
        }

        public static bool CMD_pay(Player player, string ps)
        {
            if (!ps.sscanf("ui", out object[] args))
            {
                player.SendUsageMessage("/pay [player name or id] [amount]");
                return true;
            }
            var target = args[0] as Player;
            var amount = (int)args[1];
            if (target == null)
            {
                player.SendErrorMessage("Specified player not found.");
                return true;
            }
            if (target == player)
            {
                player.SendErrorMessage("You cannot pay yourself.");
                return true;
            }
            if (amount < 1)
            {
                player.SendErrorMessage("The amount must not be lower than $1.");
                return true;
            }
            if (amount > player.Money)
            {
                player.SendErrorMessage("You don't have that much money.");
                return true;
            }
            player.Money -= amount;
            target.Money += amount;
            player.SendSuccessMessage($"You have given ${amount} to {target.Client.name}");
            target.SendInfoMessage($"{player.Client.name} has given you ${amount}");
            return true;
        }

        public static bool CMD_withdraw(Player player, string ps)
        {
            if (!player.IsInBank())
            {
                player.SendErrorMessage("You must be inside a bank to make a withdrawal.");
                return true;
            }
            if (!ps.sscanf("i", out object[] args))
            {
                player.SendUsageMessage("/withdraw [amount]");
                return true;
            }
            var amount = (int) args[0];
            if (amount < 1)
            {
                player.SendErrorMessage("The amount cannot be less than $1.");
                return true;
            }
            if (amount > player.BankMoney)
            {
                player.SendErrorMessage("You don't have that much money in your bank account.");
                return true;
            }
            player.BankMoney -= amount;
            player.Money += amount;
            player.SendSuccessMessage($"You have withdrawn ${amount}");
            return true;
        }

        public static bool CMD_deposit(Player player, string ps)
        {
            if (!player.IsInBank())
            {
                player.SendErrorMessage("You must be in a bank to make a deposit.");
                return true;
            }
            if (!ps.sscanf("i", out object[] args))
            {
                player.SendUsageMessage("/deposit [amount]");
                return true;
            }
            var amount = (int) args[0];
            if (amount < 1)
            {
                player.SendErrorMessage("The amount cannot be less than $1.");
                return true;
            }
            if (amount > player.Money)
            {
                player.SendErrorMessage("You don't have that much money.");
                return true;
            }
            player.Money -= amount;
            player.BankMoney += amount;
            player.SendSuccessMessage($"You have deposited ${amount}");
            return true;
        }

        public static bool CMD_balance(Player player, string ps)
        {
            player.SendInfoMessage($"You currently have ${player.BankMoney} in your bank account.");
            return true;
        }

        public static bool CMD_atm(Player player, string ps)
        {
            if (!player.IsNearAtm())
            {
                player.SendErrorMessage("You must be near an ATM.");
                return true;
            }
            player.GetBrowserByName("main")?.SetURL(Player.BASE_BROWSER_PATH + "atm/main.html", true, true);
            //player.GetBrowserByName("main")?.Show();
            return true;
        }

        public static bool CMD_transfer(Player player, string ps)
        {
            if (!player.IsInBank())
            {
                player.SendErrorMessage("You must be in a bank to transfer funds.");
                return true;
            }
            if (!ps.sscanf("ui", out object[] args))
            {
                player.SendUsageMessage("/transfer [player name or id] [amount]");
                return true;
            }
            var target = args[0] as Player;
            var amount = (int) args[1];
            if (target == null)
            {
                player.SendErrorMessage(CommonMessages.PLAYER_NOT_FOUND);
                return true;
            }
            if (target == player)
            {
                player.SendErrorMessage("You cannot transfer funds to yourself.");
                return true;
            }
            if (amount < 1)
            {
                player.SendErrorMessage("The amount cannot be less than $1.");
                return true;
            }
            if (amount > player.BankMoney)
            {
                player.SendErrorMessage("You don't have that much money in your bank account.");
                return true;
            }
            player.BankMoney -= amount;
            target.BankMoney += amount;
            player.SendSuccessMessage($"You have transferred ${amount} to {target.Client.name}'s bank account.");
            target.SendInfoMessage($"{player.Client.name} has transferred ${amount} to your bank account.");
            return true;
        }

        public static bool CMD_power(Player player, string ps)
        {
            if (player.Group != PlayerGroup.Admin)
            {
                player.SendCommandPermissionDenied();
                return true;
            }
            var veh = player.GetVehicle();
            if (veh == null) return true;
            veh.vehicle.enginePowerMultiplier = 100f;
            veh.vehicle.engineTorqueMultiplier = 100f;
            return true;
        }

        public static bool CMD_testcar(Player player, string ps)
        {
            var veh = new Vehicle(VehicleHash.Zentorno, player.Client.name);
            veh.Create(player.Client.position, new Vector3(0, 0, 0));
            veh.EngineStatus = true;
            player.PutInVehicle(veh);
            return true;
        }
        public static bool CMD_atele(Player player, string ps)
        {
            if (player.Group != PlayerGroup.Admin)
            {
                player.SendCommandPermissionDenied();
                return true;
            }
            if (!ps.sscanf("i", out object[] args))
            {
                player.SendInfoMessage("Current administrative teleports:");
                for (var i = 0; i < Administration.Teleports.Count; i++)
                {
                    player.SendClientMessage($"Name: {Administration.Teleports[i].Item1} - {i}");
                }
                player.SendUsageMessage("/atele [teleport id]");
                return true;
            }
            int teleportIndex = (int)args[0];
            if (teleportIndex < 0 || teleportIndex >= Administration.Teleports.Count)
            {
                player.SendErrorMessage("Teleport was not found");
                return true;
            }
            player.Client.position = Administration.Teleports[teleportIndex].Item2;
            player.SendSuccessMessage("Teleported successfully.");
            return true;
        }

        public static bool CMD_createmarker(Player player, string ps)
        {
            var vehicle = new Vehicle(VehicleHash.Zentorno, player.Client.name);
            vehicle.Create(player.Client.position, new Vector3());
            player.PersonalVehicles.Add(vehicle);
            return true;
        }

        public static bool CMD_createmarker2(Player player, string ps)
        {
            API.shared.createMarker(0, player.Client.position, new Vector3(player.Client.position.X, player.Client.position.Y, player.Client.position.Z + 50), new Vector3(), new Vector3(1, 1, 1), 255,
                    255, 0, 0, 0);
            return true;
        }

        /*public static bool CMD_register(Player player, string ps)
        {
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendClientMessage($"Usage: /register [password]");
                return true;
            }
            if (AuthenticationManager.Exists(player))
            {
                player.SendClientMessage($"You already have an account.");
                return true;
            }
            var password = args[0] as string;
            if (password.Length < 3)
            {
                player.SendClientMessage($"Password must at least be 3 characters long");
                return true;
            }
            AuthenticationManager.Register(player, password);
            player.Client.freeze(false);
            player.IsLoggedIn = true;
            player.SendClientMessage($"Registered successfully.");
            return true;
        }*/

        public static bool CMD_login(Player player, string ps)
        {
            player.DisableChat();
            player.GetBrowserByName("main").SetURL(Player.BASE_BROWSER_PATH + "auth/login.html", true, true);
            return true;
        }

        /*public static bool CMD_login(Player player, string ps)
        {
            if (player.IsLoggedIn)
            {
                player.SendErrorMessage("You are already logged in.");
                return true;
            }
            if (!player.IsReady)
            {
                player.SendErrorMessage("You have not yet loaded.");
                return true;
            }
            if (!ps.sscanf("s", out object[] args))
            {
                player.SendClientMessage("Usage: /login [password]");
                return true;
            }
            var password = args[0] as string;
            if (!AuthenticationManager.Exists(player))
            {
                player.SendClientMessage($"You do not have an account. Please /register first.");
            }
            else
            {
                if (AuthenticationManager.AttemptLogin(player, password))
                {
                    player.SendClientMessage($"~g~You have logged in!");
                    player.IsLoggedIn = true;
                    player.Client.freeze(false);
                    player.LoadPersonalVehicles();
                    player.Load();
                    player.OnLogin();
                }
                else
                {
                    player.SendClientMessage($"~r~Incorrect password.");
                }
            }
            return true;
        }*/
    }
}
