﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using eastLIFEV;
using eastLIFEV.Modules.Server;
using VineRP.Modules.Util;
using GTANetworkServer;
using GTANetworkShared;
using System.Windows.Forms;

namespace VineRP
{
    public class Player
    {
        public const string Table = "players";
        public const string TableUCP = "users";
        public const string TableInventory = "playerinventories";

        public const string BASE_BROWSER_PATH = "client/modules/world/player/ui/";

        public const float BASE_TALK_RANGE = 10f;

        public static readonly List<Tuple<Vector3, Vector3>> NewbieSpawnLocations = new List<Tuple<Vector3, Vector3>>()
        {
            new Tuple<Vector3, Vector3>(new Vector3(-1035.157, -2505.545, 13.86959), new Vector3(0, 0, -122.7621)),
            new Tuple<Vector3, Vector3>(new Vector3(-1028.916, -2493.719, 13.88725), new Vector3(0, 0, -125.7811)),
            new Tuple<Vector3, Vector3>(new Vector3(-1024.583, -2486.528, 13.88193), new Vector3(0, 0, -120.6602)),
            new Tuple<Vector3, Vector3>(new Vector3(-1020.517, -2479.376, 13.88369), new Vector3(0, 0, -124.731))
        };

        public static Dictionary<int, Player> TakenPlayerIds = new Dictionary<int, Player>();

        public static List<Player> All = new List<Player>();
        public int Sqlid { get; private set; }
        public Client Client;
        public NetHandle AmeLabel;

        public int VehicleSeat => (Client.isInVehicle) ? API.shared.getPlayerVehicleSeat(Client) : -2;

        public bool IsDriver => (VehicleSeat == -1);

        private int _Money;
        public int Money
        {
            get { return _Money; }
            set
            {
                _Money = value;
                TriggerEvent(PlayerEvents.MONEY_UPDATE, value);
            }
        }

        public string Name
        {
            get { return Client.name; }
            set { Client.name = value; }
        }

        public int ID;
        public int UCPUserID;
        public bool InVehicle => Client.isInVehicle;
        public bool IsConnected;
        public bool IsLoggedIn;
        public bool IsReady;
        public bool IsSpawnedIn;
        public Faction Faction;
        public Cache Cache;
        public int BankMoney;
        public PlayerGroup Group;
        public int Level;
        public uint PhoneNumber;
        /* Status of the phone. false = off */
        public bool PhoneStatus;
        public string SocialSecurityNumber;
        public uint MinutesPlayed;
        public Vehicle DealershipShowroomVehicle;
        public int CurrentJobId;
        public Faction OwnedFaction;
        /* Represents a faction rank, stored as a sqlid */
        public int FactionRank;
        public List<int> FactionInvites = new List<int>();
        public Inventory Inventory;
        public VTimer MinuteTimer;
        public bool IsNewbie;
        public PlayerGender Gender;

        public int CurrentBlipCounter;
        public int CurrentMarkerCounter;
        public Menu CurrentMenu;

        public List<PlayerBrowser> Browsers = new List<PlayerBrowser>();

        public bool ScoreboardShown;
        public bool ScoreboardPopulated;
        public VTimer ScoreboardUpdateTimer;

        public Vehicle RentVehicle;

        /* Garbage job */
        public Vehicle GarbageJobVehicle;
        public int GarbageLocationSet;
        public int LastGarbageLocationSet = -1;
        public int CurrentGarbageLocation;
        public Blip CurrentGarbageBlip;
        public Marker CurrentGarbageMarker;

        /* Trucking job */
        public Vehicle TruckingJobVehicle;
        public int TruckingLocationIndex;
        public DeliveryType TruckingDeliveryType;
        public bool HasTruckingPackage;
        public int CurrentDeliveryLocation;
        public Blip CurrentTruckingPackageBlip;
        public Marker CurrentTruckingPackageMarker;

        /* LSPD job */
        public bool IsOnLSPDDuty;
        public bool PTraceCoolDownExpired;
        public Blip PTraceBlip;

        public List<Vehicle> PersonalVehicles = new List<Vehicle>();

        public Player(Client client)
        {
            this.Client = client;
            IsLoggedIn = false;
            IsReady = false;
            Cache = new Cache();
            CurrentBlipCounter = 0;
            CurrentMenu = null;
            ID = -1;
        }

        public static bool operator ==(Player p1, Player p2)
        {
            if (object.ReferenceEquals(p1, null) && object.ReferenceEquals(p2, null))
            {
                return true;
            }
            if (object.ReferenceEquals(p1, null) || object.ReferenceEquals(p2, null))
            {
                return false;
            }
            return p1.ID == p2.ID;
        }

        public static bool operator !=(Player p1, Player p2)
        {
            if (object.ReferenceEquals(p1, null) && object.ReferenceEquals(p2, null))
            {
                return false;
            }
            if (object.ReferenceEquals(p1, null) || object.ReferenceEquals(p2, null))
            {
                return true;
            }
            return p1.ID != p2.ID;
        }

        public static Player Get(string identifier)
        {
            int id;
            if (int.TryParse(identifier, out id))
            {
                return Player.All.FirstOrDefault(p => p.ID == id);
            }
            else
            {
                return Player.All.FirstOrDefault(p => p.Client.name.ToLower().StartsWith(identifier.ToLower()));
            }
        }

        public static Player GetPlayerFromClient(Client client)
        {
            return Player.All.Find(p => p.Client.name == client.name);
        }

        public static Player GetPlayerFromNetHandle(NetHandle player)
        {
            return Player.All.Find(p => p.Client.handle == player);
        }

        public void PutInVehicle(Vehicle vehicle, int seat = -1)
        {
            API.shared.setPlayerIntoVehicle(Client, vehicle.vehicle.handle, seat);
        }

        public void RemoveFromVehicle()
        {
            if (GetVehicle() != null)
            {
                API.shared.warpPlayerOutOfVehicle(Client, GetVehicle().vehicle.handle);
            }
        }

        public void TriggerEvent(string eventName, params object[] args)
        {
            API.shared.triggerClientEvent(Client, eventName, args);
        }

        public void SetWaypoint(float x, float y)
        {
            API.shared.sendNativeToPlayer(Client, GTANetworkServer.Hash.SET_NEW_WAYPOINT, x, y);
        }

        public void SetSyncedData(string key, object value)
        {
            API.shared.setEntitySyncedData(Client, key, value);
        }

        public dynamic GetSyncedData(string key)
        {
            return API.shared.getEntitySyncedData(Client.handle, key);
        }

        public void OnConnected()
        {
            IsConnected = true;
            if (!Client.isCEFenabled)
            {
                SendInfoMessage("This server requires CEF.");
                SendInfoMessage("The option to enable CEF can be found in GTA:Network settings.");
                Client.kick();
                return;
            }

            #region Ban check
            /* Checking if the player is banned */
            var query = new DBQuery($"SELECT * FROM `banlist` WHERE name='{Name}' AND bannedUntil > '{Time.GetUnixTimestamp()}' LIMIT 1");
            query.Execute();

            if (query.GetResult().HasRows)
            {
                query.GetResult().Read();
                var reason = query.GetResult().GetString("reason");
                var bannedUntil = Time.UnixTimestampToDateTime(query.GetResult().GetInt32("bannedUntil"));
                var id = query.GetResult().GetInt32("id");
                SendCustomMessage("Ban", Color.Red.RGB(), $"You are banned until {bannedUntil:F} for '{reason}' (ID: {id})");
                Client.kick();
                query.Close();
                return;
            }

            query.Close();
            #endregion

            /*var timer = new VTimer(2500, false);
            timer.AddPlayerRelationship(this);
            timer.Elapsed += (sender, args) =>
            {
                Browser.CreateBrowser(0, 1920, 1080);
            };

            timer.Start();*/

            Log.Custom("CONNECTED", ConsoleColor.Green, $"Player ({Client.name}) SC ({Client.socialClubName}) has connected.");
            Client.freeze(true);
            //SendInfoMessage("Please log in to your account using /login [password]");
            //SendInfoMessage("Don't have an account? Visit vinerp.com");

            /*
                TODO
                Should trigger the sign in page. After signin the player should then select their character.
            */

            if(GlobalSettings.ANNOUNCE_CONNECTIONS)
                Player.SendMessageToAll($"{Color.Aqua.RGB()}Player {Name} has connected.");

            /*var authTimer = new VTimer(5000, false);

            authTimer.Elapsed += (sender, args) =>
            {
                SetBrowserURL(Player.BASE_BROWSER_PATH + "auth/login.html", true, true);
            };

            authTimer.Start();*/
        }


        /*
         * This is called once the player begins connecting to the server.
         */
        public void OnBeginConnect()
        {
        }

        /*
         * This is called once the player logs in successfully.
         */
        public void OnLogin()
        {
            /* Populate the current players' scoreboard with other players */
            SendDebugMessage("Populating your scoreboard");
            var scoreboardPopTimer = new VTimer(2000, false);
            scoreboardPopTimer.AddPlayerRelationship(this);

            scoreboardPopTimer.Elapsed += (sender, args) =>
            {
                Log.Debug("Populating scoreboard for player " + Name);
                foreach (var player in Player.All)
                {
                    Log.Debug("Populating player " + player.Name);
                    if (!player.IsLoggedIn) continue;
                    Log.Debug("Player is logged in, add");
                    AddPlayerToScoreboard(player.Name, player.Client.ping, player.ID);
                }
                ScoreboardPopulated = true;
            };

            scoreboardPopTimer.Start();

            SendDebugMessage("Adding this player to other players' scoreboard");

            /* Add current player to other players scoreboard */
            foreach (var target in Player.All)
            {
                if (target.ID == ID) continue;
                if (!target.IsLoggedIn) continue;
                if (!target.ScoreboardPopulated) continue;

                target.AddPlayerToScoreboard(Name, Client.ping, ID);
            }

            /* The problem is most likely here */
            /* Log.Debug("Adding player " + Name + " to other players' scoreboards");
            foreach (var player in Player.All)
            {
                Log.Debug("Trying player " + player.Name);
                if (player != this)
                {
                    if (!player.IsLoggedIn) return;

                    if (player.ScoreboardPopulated)
                    {
                        player.AddPlayerToScoreboard(Name, Client.ping, ID);
                        Log.Debug("Added player " + Name + " to the scoreboard of " + player.Name);
                    }
                }
            }*/

            SendDebugMessage("Starting scoreboard update");
            ScoreboardUpdateTimer = new VTimer(GlobalSettings.SCOREBOARD_UPDATE_INTERVAL, true);
            ScoreboardUpdateTimer.AddPlayerRelationship(this);

            ScoreboardUpdateTimer.Elapsed += ScoreboardUpdate;

            ScoreboardUpdateTimer.Start();
        }

        private void ScoreboardUpdate(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var player in All)
            {
                if (!player.IsLoggedIn) continue;
                UpdatePlayerInScoreboard(player.Name, player.Client.ping);
            }
        }

        /*
         * This is called once the data has been loaded from the database.
         */
        public void OnDataLoaded()
        {
            MinuteTimer = new VTimer(1000 * 60, true);
            MinuteTimer.AddPlayerRelationship(this);

            MinuteTimer.Elapsed += (sender, args) =>
            {
                MinutesPlayed++;
                if(((int)(MinutesPlayed / 60)) >= GetRequiredRankupHours())
                {
                    Level++;
                    SendInfoMessage("You have leveled up to level " + Level);
                }
            };

            MinuteTimer.Start();
        }
        
        /*
         * This is called once the player finishes downloading the client-side assets.
         */
        public void OnFinishedDownload()
        {
            Log.log($"Player {Client.name} has finished downloading");
        }
        
        /*
         * This is called once the player disconnects from the server.
         */
        public void OnDisconnected(string reason)
        {
            IsConnected = false;
            if(GlobalSettings.ANNOUNCE_CONNECTIONS)
                Player.SendMessageToAll($"{Color.Aqua.RGB()}Player {Name} has disconnected.");
            Log.Custom("DISCONNECTED", ConsoleColor.Yellow, $"Player ({Client.name}) SC ({Client.socialClubName}) has disconnected.");
            if (!IsLoggedIn) return;

            if (DealershipShowroomVehicle != null)
            {
                if (DealershipShowroomVehicle.vehicle != null) DealershipShowroomVehicle.Dispose(true);
                DealershipShowroomVehicle = null;
            }

            if (RentVehicle != null)
            {
                if (RentVehicle.vehicle != null) RentVehicle.Dispose(true);
                RentVehicle = null;
            }

            GarbageJobVehicle?.Dispose(true);

            /* If a player has spawned, personal vehicles, save them and dispose */
            if (PersonalVehicles.Count > 0)
            {
                foreach (var personalVehicle in PersonalVehicles)
                {
                    if (personalVehicle.IsCreated() && !personalVehicle.IsDestroyed)
                    {
                        personalVehicle.Save(true);
                    }
                }
            }
            Save();

            ScoreboardUpdateTimer?.Dispose();

            /* Remove current player from other players' scoreboard */
            foreach (var player in All)
            {
                if (player == this) continue;
                if (!player.IsLoggedIn) continue;
                player.RemovePlayerFromScoreboard(Name);
            }


            ///* Release the player ID */
            //TakenPlayerIds.Remove(ID);
        }

        /*
         * This is called once the player enters a vehicle.
         */
        public void OnEnterVehicle(Vehicle vehicle)
        {
            Log.Debug($"Player {Client.name} entered vehicle with seat id {API.shared.getPlayerVehicleSeat(Client)}");
            if (!vehicle.OwnerName.Equals(Client.name) && vehicle.Locked)
            {
                SendInfoMessage("This vehicle is locked.");
                Client.warpOutOfVehicle(vehicle.vehicle);
                return;
            }
            SendCustomMessage("DEBUG", Color.Aqua.RGB(), $"You entered a vehicle. Seat id: {VehicleSeat}");

            if(vehicle.vehicle.handle != DealershipShowroomVehicle?.vehicle.handle)
                SendInfoMessage("Use /engine to turn your vehicle's engine on/off.");


            if (GarbageJobVehicle.vehicle.handle == vehicle.vehicle.handle)
            {
                TriggerEvent("modules_faction_garbage_vehicle_enter", GarbageJobVehicle.Cache.Get("faction_garbage_current_garbage_count"), FactionGarbage.MAX_GARBAGE_PER_TRUCK);
            }

            if (vehicle.IsJobVehicle())
            {
                if (vehicle.DespawnTimer.IsStarted())
                {
                    SendDebugMessage("You have entered a job vehicle, stopping existing despawn timer.");
                    vehicle.DespawnTimer.Stop();
                    vehicle.DespawnTimer.Dispose();
                }
            }
        }

        /*
         * This is called once the player exists a vehicle.
         */
        public void OnExitVehicle(Vehicle vehicle)
        {
            if (vehicle.IsJobVehicle())
            {
                SendDebugMessage("You have exited a job vehicle, starting the despawn timer. (5 mins)");
                vehicle.DespawnTimer = new VTimer(1000 * 60 * 5, false);

                vehicle.DespawnTimer.Elapsed += (sender, args) =>
                {
                    vehicle.Dispose(true);
                };

                vehicle.DespawnTimer.Start();
            }
        }

        /*
         * This is called once the player dies.
         */
        public void OnDeath(NetHandle killer, int weapon)
        {
        }

        /*
         * This is called once the player respawns.
         */
        public void OnRespawn()
        {
        }

        /*
         * This is called once a player presses a key.
         */
        private void OnKeyDown(int keyValue, bool isChatOpen, bool control, bool shift)
        {
            if (!IsLoggedIn) return;
            if (!isChatOpen)
            {
                //SendDebugMessage("Clicked " + keyValue);
                if (InVehicle)
                {
                    if (keyValue == VKeys.COMMA) // comma
                    {
                        GetVehicle().ToggleIndicator(VehicleIndicators.Left);
                    }
                    else if (keyValue == VKeys.PERIOD) // period
                    {
                        GetVehicle().ToggleIndicator(VehicleIndicators.Right);
                    }
                }
                if (keyValue == VKeys.TAB)
                {
                    ScoreboardShown = !ScoreboardShown;
                    if (ScoreboardShown)
                    {
                        GetBrowserByName("scoreboard")?.Show(true);
                    }
                    else
                    {
                        GetBrowserByName("scoreboard")?.Hide(true);
                    }
                }
            }
        }

        /*
         * This is called once the player is ready, e.g. all client-side scripts are downloaded.
         */
        private void OnReady()
        {
            IsReady = true;
            SendDebugMessage("You are ready.");
            CreateBrowsers();
            SendCustomMessage("VineRP", Color.Aqua.RGB(), "Welcome to VineRP!");
            SendInfoMessage("To login, use the /login command.");
            SendInfoMessage("You have 2 minutes to log in, otherwise you will be kicked.");
            SendInfoMessage("Don't have an account? Visit www.vinerp.com");

            TriggerEvent(PlayerEvents.AUTH_START_CAMERA);

            var authenticationTimer = new VTimer(120 * 1000, false);
            authenticationTimer.AddPlayerRelationship(this);

            authenticationTimer.Elapsed += (sender, args) =>
            {
                if (!IsLoggedIn)
                {
                    Client.kick("You did not log in in time.");
                }
            };

            authenticationTimer.Start();
        }

        public void OnEventTrigger(string eventName, object[] args)
        {
            if (eventName == PlayerEvents.EVENT_ONKEYDOWN)
            {
                OnKeyDown((int)args[0], (bool)args[1], (bool)args[2], (bool)args[3]);
            }
            else if (eventName == PlayerEvents.EVENT_READY_HEARTBEAT)
            {
                this.OnReady();
            }
        }

        public void OnChatMessage(string message)
        {
            if (!IsLoggedIn)
            {
                SendErrorMessage($"You must be logged in to talk.");
            }
            else
            {
                /* TODO: Check if the user is in call */
                string chatMessage = $"{Client.name} says: {message}";

                /* If the player is in a vehicle and the windows are up, send the message to the passengers only */
                if (InVehicle && !(GetVehicle().WindowsDown))
                {
                    Player.SendMessageToPlayers(GetVehicle().Occupants, chatMessage);
                }
                else
                {
                    Player.SendRangedMessage(Color.White.RGB(), chatMessage, Client.position, Player.BASE_TALK_RANGE);
                }
            }
        }

        public void OnEnterColShape(ColShape colShape)
        {
            SendDebugMessage("Enter col shape");
        }

        public void DisableChat()
        {
            TriggerEvent(PlayerEvents.CHAT_DISABLE);
        }

        public void EnableChat()
        {
            TriggerEvent(PlayerEvents.CHAT_ENABLE);
        }

        public void AssignUniqueId()
        {
            Log.Debug("Trying to assign an ID for player " + Name);
            int foundUniqueId = -1;
            for (int i = 0; i < GlobalSettings.MAX_PLAYERS; i++)
            {
                if (!TakenPlayerIds.ContainsKey(i))
                {
                    Log.Debug("Assigned ID " + i);
                    foundUniqueId = i;
                    break;
                }
                else
                {
                    Log.Debug("The ID " + i + " is unavailable, trying others.");
                }
            }
            if (foundUniqueId == -1)
            {
                Log.Error($"Unable to assign player {Name} a unique ID!");
                return;
            }
            ID = foundUniqueId;
            TakenPlayerIds.Add(foundUniqueId, this);
        }

        private void CreateBrowsers()
        {
            new PlayerBrowser(this, "main", ResolutionType.FullResolution);
            new PlayerBrowser(this, "scoreboard", ResolutionType.FullResolution).
                SetURL("client/modules/world/player/ui/scoreboard/index.html", false, false);
            new PlayerBrowser(this, "customization", ResolutionType.FullResolution).
                SetURL("client/modules/world/player/ui/customization/index.html", false, false);
        }

        public PlayerBrowser GetBrowserByName(string name)
        {
            var result = Browsers.FirstOrDefault(b => b.Name == name);
            if(result == null) Log.Error($"Got null when trying to get a browser by name {name}");
            return result;
        }

        public void CloseMenu()
        {
            CurrentMenu?.Close();
        }

        public Vehicle GetVehicle()
        {
            if (InVehicle)
            {
                return Vehicle.All.FirstOrDefault(v => v.originalVehicle.handle == Client.vehicle.handle);
            }
            return null;
        }

        public bool IsInArea(float xmin, float ymin, float xmax, float ymax)
        {
            return (Client.position.X > xmin && Client.position.Y > ymin && Client.position.X < xmax &&
                    Client.position.Y < ymax);
        }

        public static uint GeneratePhoneNumber()
        {
            uint result = 0;
            do
            {
                // Generate a random number, 7 digits long
                uint randomPhoneNumberTry = (uint) Random.GetInt(1111111, 9999999);
                // Check if the phone number doesn't exist in the database
                var query = new DBQuery($"SELECT * FROM `{Table}` WHERE phoneNumber='{randomPhoneNumberTry}' LIMIT 1", QueryType.QUERY);
                query.Execute();
                if (!query.GetResult().HasRows)
                {
                    result = randomPhoneNumberTry;
                }
                query.Close();

            } while (result == 0);
            return result;
        }

        private static string GenerateSocialSecurityNumber()
        {
            var result = "";
            do
            {
                /* Generate a random SSN */
                string randomSsnTry = $"{Random.GetInt(110, 1000)}-{Random.GetInt(110, 1000)}-{Random.GetInt(1110, 9999)}";
                /* Checking if the generated SSN is unused */
                var query = new DBQuery($"SELECT * FROM `{Table}` WHERE ssn='{randomSsnTry}' LIMIT 1", QueryType.QUERY);
                query.Execute();
                if (!query.GetResult().HasRows)
                {
                    result = randomSsnTry;
                }
                query.Close();
            } while (result == "");
            return result;
        }

        public bool IsNearAtm(float range = 1f)
        {
            foreach (var atmLocation in EconomyManager.ATMLocations)
            {
                if (atmLocation.DistanceTo(Client.position) <= range)
                    return true;
            }
            return false;
        }

        public bool IsInBank(float range = 2.5f)
        {
            foreach (var bankLocation in EconomyManager.BankLocations)
            {
                if (bankLocation.DistanceTo(Client.position) <= range)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Returns the required hours of playtime to rank up.
        /// </summary>
        /// <returns></returns>
        public int GetRequiredRankupHours()
        {
            return 8 + (Level-1 * 4);
        }

        /// <summary>
        /// Saves the players data into the database.
        /// </summary>
        public void Save()
        {
            if (!IsLoggedIn) return;

            var query = new DBQuery($"UPDATE `{Player.Table}` SET posX='{Client.position.X}', posY='{Client.position.Y}', posZ='{Client.position.Z}', moneyBank='{BankMoney}' WHERE name='{Name}'", QueryType.INSERT_OR_DELETE);
            query.Execute();
            query = new DBQuery($"UPDATE `{Player.Table}` SET money='{Money}', level='{Level}', ownedFaction='{OwnedFaction?.ID ?? 0}', aGroup='{((int)Group)}', minutesPlayed='{MinutesPlayed}' WHERE name='{Name}'", QueryType.INSERT_OR_DELETE);
            query.Execute();
            query = new DBQuery($"UPDATE `{Player.Table}` SET phoneNumber='{PhoneNumber}', ssn='{SocialSecurityNumber}', newbie='0', phoneStatus='{((PhoneStatus) ? 1 : 0)}' WHERE name='{Name}'", QueryType.INSERT_OR_DELETE);
            query.Execute();

            SaveInventory();
        }

        /// <summary>
        /// Save the players' inventory in the database.
        /// </summary>
        private void SaveInventory()
        {
            if (Inventory == null) return;
            if (Inventory.GetUsedSlots() < 1) return;

            /* The save format for an item is ItemID:ItemQuantity, seperated by '|' */
            string inventorySaveString = "";

            foreach (Tuple<Item, dynamic> item in Inventory.GetAllItems())
            {
                if (inventorySaveString.Length > 0) inventorySaveString += '|';
                inventorySaveString += $"{item.Item1.ID}:{item.Item2}";
            }

            if (DoesInventoryRecordExist())
            {
                new DBQuery($"UPDATE `{TableInventory}` SET inventory='{inventorySaveString}' WHERE userId='{Sqlid}'",
                    QueryType.INSERT_OR_DELETE).Execute();
            }
            else
            {
                new DBQuery($"INSERT INTO `{TableInventory}` (userId, inventory) VALUES ('{Sqlid}', '{inventorySaveString}')", QueryType.INSERT_OR_DELETE).Execute();
            }
        }

        /// <summary>
        /// Checks whether the player has an inventory table record.
        /// </summary>
        public bool DoesInventoryRecordExist()
        {
            var query = new DBQuery($"SELECT * FROM `{TableInventory}` WHERE userId='{Sqlid}' LIMIT 1", QueryType.QUERY);
            query.Execute();
            if (query.GetResult().HasRows)
            {
                query.Close();
                return true;
            }
            query.Close();
            return false;
        }

        /// <summary>
        /// Loads the players' inventory from the database.
        /// </summary>
        private void LoadInventory()
        {
            Inventory = new Inventory(Inventory.BASE_PLAYER_SLOTS);

            if (!DoesInventoryRecordExist()) return;

            var query = new DBQuery($"SELECT * FROM `{TableInventory}` WHERE userId='{Sqlid}' LIMIT 1", QueryType.QUERY);
            query.Execute(true);

            var inventoryString = query.GetResult().GetString("inventory");
            var allInventoryItems = inventoryString.Split('|');
            Log.Debug("Found " + allInventoryItems.Length + " inventory items");
            Log.Debug("A " + allInventoryItems[0]);

            foreach (var inventoryItem in allInventoryItems)
            {
                if (string.IsNullOrEmpty(inventoryItem)) continue;
                var itemData = inventoryItem.Split(':');
                Log.Debug("Item data count: " + itemData.Length);

                /* Checking if the quantity is a float or an int
                   Even though Inventory.Give accepts dynamic type it still must be an int or a float
                 */
                if (itemData[1].Contains('.'))
                    Inventory.Give(Item.GetItemById(int.Parse(itemData[0])), float.Parse(itemData[1]));
                else
                    Inventory.Give(Item.GetItemById(int.Parse(itemData[0])), int.Parse(itemData[1]));
            }

            query.Close();
        }

        /// <summary>
        /// Loads players' character data.
        /// </summary>
        public void Load(string characterName)
        {
            var query = new DBQuery($"SELECT * FROM `{Player.Table}` WHERE name='{characterName}'");
            query.Execute(true);
            if (query.GetResult().GetInt32("newbie") == 1)
            {
                Client.position = Player.NewbieSpawnLocations[Random.GetInt(0, Player.NewbieSpawnLocations.Count)]
                    .Item1;
                Client.rotation = Player.NewbieSpawnLocations[Random.GetInt(0, Player.NewbieSpawnLocations.Count)]
                    .Item2;
            }
            else
            {
                Client.position = new Vector3(query.GetResult().GetDouble("posX"), query.GetResult().GetDouble("posY"), query.GetResult().GetDouble("posZ"));
            }

            Sqlid = query.GetResult().GetInt32("id");
            Money = query.GetResult().GetInt32("money");
            //API.shared.setEntitySyncedData(Client, "test", 12345);
            BankMoney = query.GetResult().GetInt32("moneyBank");
            MinutesPlayed = query.GetResult().GetUInt32("minutesPlayed");
            Level = query.GetResult().GetInt32("level");
            OwnedFaction = (query.GetResult().GetInt32("ownedFaction") == 0)
                ? null
                : Faction.Find(query.GetResult().GetInt32("ownedFaction"));
            Faction = (query.GetResult().GetInt32("faction") == 0)
                ? null
                : Faction.Find(query.GetResult().GetInt32("faction"));
            Group = (PlayerGroup) query.GetResult().GetUInt32("aGroup");
            PhoneNumber = query.GetResult().GetUInt32("phoneNumber");
            SocialSecurityNumber = query.GetResult().GetString("ssn");
            Gender = (PlayerGender) query.GetResult().GetUInt32("gender");
            PhoneStatus = query.GetResult().GetUInt32("phoneStatus") == 1;

            query.Close();

            LoadInventory();
            //Inventory = new Inventory(15);
            //Inventory.Give(Item.FishingRod, 1);

            if (PhoneNumber == 0) PhoneNumber = GeneratePhoneNumber();
            if (SocialSecurityNumber == "") SocialSecurityNumber = GenerateSocialSecurityNumber();

            OnDataLoaded();
        }

        /// <summary>
        /// Loads players' personal vehicles from the database.
        /// </summary>
        public void LoadPersonalVehicles()
        {
            Log.Debug($"Loading personal vehicles for player {Client.name}");
            var query = new DBQuery($"SELECT * FROM `{Vehicle.Table}` WHERE Owner=@Name", QueryType.QUERY);
            query.AddParam("@Name", Client.name);
            query.Execute();
            if (query.GetResult().HasRows)
            {
                while (query.GetResult().Read())
                {
                    var vehicle = new Vehicle((VehicleHash) query.GetResult().GetInt32("model"), Client.name)
                    {
                        Sqlid = query.GetResult().GetInt32("id")
                    };
                    PersonalVehicles.Add(vehicle);
                    Log.Debug($"Found vehicle id {vehicle.Sqlid} for player {Client.name}");
                }
            }
            query.Close();
        }

        private void AddPlayerToScoreboard(string playerName, int playerPing, int playerId)
        {
            TriggerEvent(PlayerEvents.SCOREBOARD_PLAYER_ADD, playerName, playerPing, playerId);
        }

        private void RemovePlayerFromScoreboard(string playerName)
        {
            TriggerEvent(PlayerEvents.SCOREBOARD_PLAYER_REMOVE, playerName);
        }

        private void UpdatePlayerInScoreboard(string playerName, int playerPing)
        {
            TriggerEvent(PlayerEvents.SCOREBOARD_PLAYER_UPDATE, playerName, playerPing);
        }

        #region Chat Methods
        public static void SendRangedMessage(string color, string message, Vector3 point, float range)
        {
            foreach (var player in All)
            {
                if (player.Client.position.DistanceTo(point) <= range)
                {
                    API.shared.sendChatMessageToPlayer(player.Client, color, message);
                }
            }
        }

        public static void SendRangedMessage(string message, Vector3 point, float range)
        {
            foreach (var player in All)
            {
                if (player.Client.position.DistanceTo(point) <= range)
                {
                    player.SendClientMessage(message);
                }
            }
        }

        public static void SendMessageToPlayers(List<Player> players, string message)
        {
            if (players.Count < 1)
            {
                Log.Warning("Attempting to call SendMessageToPlayers for zero players.");
                return;
            }
            foreach (var player in players)
            {
                player.SendClientMessage(message);
            }
        }

        public bool SendUsageMessage(string message)
        {
            API.shared.sendChatMessageToPlayer(Client, Color.Orange.RGB(), $"Usage » {message}");
            return true;
        }

        public bool SendErrorMessage(string message)
        {
            API.shared.sendChatMessageToPlayer(Client, Color.Red.RGB(), $"Error » {message}");
            return true;
        }

        public bool SendSuccessMessage(string message)
        {
            API.shared.sendChatMessageToPlayer(Client, Color.Lime.RGB(), $"Success » {message}");
            return true;
        }

        public bool SendInfoMessage(string message)
        {
            API.shared.sendChatMessageToPlayer(Client, Color.Orange.RGB(), $"Information » {message}");
            return true;
        }

        public bool SendCommandPermissionDenied()
        {
            SendErrorMessage("You don't have permission to use this command.");
            return true;
        }

        public void SendDebugMessage(string message)
        {
            if(Main.logLevel.HasFlag(Main.LogLevel.Debug))
                API.shared.sendChatMessageToPlayer(Client, Color.Cyan.RGB(), $"Debug » {message}");
        }

        public void SendCustomMessage(string tag, string color, string message)
        {
            API.shared.sendChatMessageToPlayer(Client, color, $"{tag} » {message}");
        }

        public static void SendMessageToAll(string message)
        {
            API.shared.sendChatMessageToAll(message);
        }

        public void SendClientMessage(string message)
        {
            Client.sendChatMessage(message);
        }
        #endregion
    }
}

