﻿namespace VineRP
{
    public static class PlayerEvents
    {
        /* Browsers */
        public const string BROWSER_CREATE = "modules_player_browser_create";
        public const string BROWSER_DESTROY = "modules_player_browser_destroy";
        public const string BROWSER_SETURL = "modules_player_browser_seturl";
        public const string BROWSER_SHOW = "modules_player_browser_show";
        public const string BROWSER_HIDE = "modules_player_browser_hide";

        /* UI */
        public const string UI_ATM_SHOW_DEPOSIT = "modules_player_ui_atm_show_deposit";
        public const string UI_ATM_SHOW_WITHDRAW = "modules_player_ui_atm_show_withdraw";

        /* Chat */
        public const string CHAT_ENABLE = "modules_player_enable_chat";
        public const string CHAT_DISABLE = "modules_player_disable_chat";

        /* Authentication */
        public const string AUTH_LOGIN = "modules_player_auth_login";
        public const string AUTH_START_CAMERA = "modules_player_start_auth_camera";
        public const string AUTH_RESET_CAMERA = "modules_player_reset_auth_camera";

        /* Economy */
        public const string ECONOMY_BANK_DEPOSIT = "modules_player_economy_bank_deposit";
        public const string ECONOMY_BANK_WITHDRAW = "modules_player_economy_bank_withdraw";
        public const string ECONOMY_BANK_CHECKBALANCE = "modules_player_economy_bank_checkbalance";

        public const string MONEY_UPDATE = "modules_player_money_update";

        /* Events */
        public const string EVENT_ONKEYDOWN = "modules_player_event_onkeydown";
        public const string EVENT_READY_HEARTBEAT = "modules_players_event_ready_heartbeat";

        /* Garbage Faction */
        public const string FACTION_GARBAGE_START = "modules_faction_garbage_job_start";
        public const string FACTION_GARBAGE_UPDATE = "modules_faction_garbage_job_update";
        public const string FACTION_GARBAGE_FINISH = "modules_faction_garbage_job_finish";

        /* Trucking Faction */
        public const string FACTION_TRUCKING_START = "modules_faction_trucking_job_start";
        public const string FACTION_TRUCKING_FINISH = "modules_faction_trucking_job_finish";

        /* Vehicle */
        public const string VEHICLE_TRIGGER_INDICATOR = "modules_vehicle_trigger_indicator";

        /* Menus */
        public const string MENU_SHOW = "modules_menu_show";
        public const string MENU_ONITEMSELECT = "modules_menu_onitemselect";
        public const string MENU_ONCLOSE = "modules_menu_onclose";
        public const string MENU_CLOSE = "modules_menu_close";

        /* Blips */
        public const string BLIP_CREATE = "modules_blip_create";
        public const string BLIP_DESTROY = "modules_blip_destroy";
        public const string BLIP_SET_COLOR = "modules_blip_setcolor";
        public const string BLIP_SET_SPRITE = "modules_blip_setsprite";

        /* Markers */
        public const string MARKER_CREATE = "modules_market_create";
        public const string MARKER_DESTROY = "modules_marker_destroy";
        public const string MARKER_SET_COLOR = "modules_marker_setcolor";

        /* Scoreboard */
        public const string SCOREBOARD_PLAYER_ADD = "modules_player_scoreboard_player_add";
        public const string SCOREBOARD_PLAYER_REMOVE = "modules_player_scoreboard_player_remove";
        public const string SCOREBOARD_PLAYER_UPDATE = "modules_player_scoreboard_player_update";

        /* Customization */

        public const string CUSTOMIZATION_CAMERA = "modules_players_event_customization_camera";
        public const string CUSTOMIZATION_CAMERA_END = "modules_players_event_customization_camera_end";

    }
}