﻿using System;
using GTANetworkServer;

namespace VineRP
{
    public static class PlayerUI
    {
        public static void Init()
        {
            Main.Instance.API.onClientEventTrigger += OnPlayerEventTrigger;
        }

        private static void OnPlayerEventTrigger(Client sender, string eventName, params object[] arguments)
        {
            var player = Player.GetPlayerFromClient(sender);

            if (eventName == PlayerEvents.UI_ATM_SHOW_DEPOSIT)
            {
                player.GetBrowserByName("main").SetURL(Player.BASE_BROWSER_PATH + "atm/deposit.html", true, false);
            }
            else if (eventName == PlayerEvents.UI_ATM_SHOW_WITHDRAW)
            {
                player.GetBrowserByName("main").SetURL(Player.BASE_BROWSER_PATH + "atm/withdraw.html", true, false);
            }
        }
    }
}