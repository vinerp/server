﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VineRP
{
    public class PlayerBrowser
    {
        public string Name;
        private bool Created;
        private Player Owner;
        private ResolutionType ResolutionType;
        public int Width;
        public int Height;

        public PlayerBrowser(Player owner, string name, ResolutionType resolutionType, int width = 0, int height = 0)
        {
            Owner = owner;
            Name = name;
            Width = width;
            Height = height;
            ResolutionType = resolutionType;

            CreateBrowser();
            owner.Browsers.Add(this);
        }

        public void CreateBrowser()
        {
            if (Created) return;
            Created = true;
            Owner.TriggerEvent(PlayerEvents.BROWSER_CREATE, Name, (int)ResolutionType, Width, Height);
        }

        public void DestroyBrowser()
        {
            if (!Created) return;
            Created = false;
            Owner.TriggerEvent(PlayerEvents.BROWSER_DESTROY, Name);
        }

        public bool IsCreated()
        {
            return Created;
        }

        public void Show(bool enableCursor = false)
        {
            Owner.TriggerEvent(PlayerEvents.BROWSER_SHOW, Name, enableCursor);
        }

        public void Hide(bool hideCursor = false)
        {
            Owner.TriggerEvent(PlayerEvents.BROWSER_HIDE, Name, hideCursor);
        }

        public void Reset()
        {
            SetURL("client/modules/world/player/ui/empty.html", false, false);
        }

        public void SetURL(string url, bool enableCursor, bool show)
        {
            if (!Created)
            {
                Log.Error($"Trying to set url of a browser that is not created. Player name: {Owner.Client.name}");
                return;
            }
            Owner.TriggerEvent(PlayerEvents.BROWSER_SETURL, Name, url, enableCursor, show);
        }
    }

    public enum ResolutionType
    {
        Set = 0,
        FullResolution = 1
    }
}