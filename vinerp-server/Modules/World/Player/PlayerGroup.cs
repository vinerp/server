﻿using System;

namespace VineRP
{
    public enum PlayerGroup
    {
        None,
        Helper,
        Admin
    }
}