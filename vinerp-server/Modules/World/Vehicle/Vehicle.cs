﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eastLIFEV.Modules.Server;
using GTANetworkShared;
using GTANetworkServer;

namespace VineRP
{
    public class Vehicle
    {
        public const string Table = "vehicles";
        public const int MAX_PLAYER_VEHICLES = 5;
        public static List<Vehicle> All = new List<Vehicle>();


        public GTANetworkServer.Vehicle vehicle;
        /* This is a copy of 'vehicle' so that the OnVehicleDeath event can be called successfully. */
        public GTANetworkServer.Vehicle originalVehicle;
        public string OwnerName;
        public VehicleHash Model;
        public bool IsDestroyed;
        public int Sqlid;
        private bool _WindowsDown;
        public int Color1, Color2;
        public int JobID = -1;
        public Cache Cache = new Cache();
        /* Vehicle despawn timer for faction cars */
        public VTimer DespawnTimer;
        /* Keeps track of the indicator statuses */
        public bool LeftIndicator;
        public bool RightIndicator;
        public bool Locked;

        public bool EngineStatus
        {
            set { API.shared.setVehicleEngineStatus(vehicle, value); }
            get { return API.shared.getVehicleEngineStatus(vehicle); }
        }

        public bool BulletproofTyres
        {
            set { API.shared.setVehicleBulletproofTyres(vehicle, value); }
            get { return API.shared.getVehicleBulletproofTyres(vehicle); }
        }

        public float Health
        {
            set { API.shared.setVehicleHealth(vehicle, value); }
            get { return API.shared.getVehicleHealth(vehicle); }
        }

        public Vector3 Position
        {
            get { return API.shared.getEntityPosition(vehicle); }
            set { API.shared.setEntityPosition(vehicle, value); }
        }

        public int Dimension
        {
            get
            {
                if (!IsCreated()) return -1;
                return API.shared.getEntityDimension(vehicle.handle);
            }
            set
            {
                if (!IsCreated()) return;
                API.shared.setEntityDimension(vehicle.handle, value);
            }
        }

        public string NumberPlate
        {
            get { return API.shared.getVehicleNumberPlate(vehicle.handle); }
            set { API.shared.setVehicleNumberPlate(vehicle.handle, value); }
        }

        public bool WindowsDown
        {
            get { return _WindowsDown; }
            set
            {
                _WindowsDown = value;
                if (!IsCreated()) return;
                if (value)
                {
                    API.shared.sendNativeToAllPlayers(GTANetworkServer.Hash.ROLL_DOWN_WINDOWS, vehicle);
                }
                else
                {
                    for(int i = 0; i < 4; i++)
                        API.shared.sendNativeToAllPlayers(GTANetworkServer.Hash.ROLL_UP_WINDOW, vehicle, i);
                }
            }
        }

        public string DisplayName
        {
            get
            {
                if (IsCreated())
                    return API.shared.getVehicleDisplayName(Model);
                else
                    return Model.ToString();
            }
        }

        public List<Player> Occupants
        {
            get
            {
                var occupants = new List<Player>();
                foreach(Client o in API.shared.getVehicleOccupants(vehicle))
                {
                    occupants.Add(Player.GetPlayerFromClient(o));
                }
                return occupants;
            }
        }

        public bool IsCreated()
        {
            return vehicle != null;
        }

        public Vehicle(VehicleHash model, string ownerName = "")
        {
            OwnerName = ownerName;
            vehicle = null;
            originalVehicle = null;
            Model = model;
            JobID = 0;
        }

        public Vehicle(VehicleHash model, int jobid) : this(model, "job")
        {
            JobID = jobid;
        }

        public bool IsJobVehicle()
        {
            return JobID > 0;
        }

        public static Vehicle GetVehicleFromNetHandle(NetHandle veh)
        {
           /* Vehicle res = null;
            Log.Debug($"Currently {All.Count} vehicles registered");
            for (int i = 0; i < All.Count; i++)
            {
                Log.Debug($"Iter {i}");
                if (All[i].originalVehicle.handle == veh)
                {
                    Log.Debug($"Match {i}");
                    res = All[i];
                }
            }
            return res;*/
            return Vehicle.All.FirstOrDefault(v => v.originalVehicle.handle == veh);
        }

        public void Create(Vector3 position, Vector3 rotation, int dimension = 0)
        {
            vehicle = API.shared.createVehicle(Model, position, rotation, Color1, Color2, dimension);
            originalVehicle = vehicle.Copy();
            EngineStatus = false;
            WindowsDown = true;
            All.Add(this);
        }

        public void Save(bool destroyFromWorld = false)
        {
            /* Checking if the vehicle has an owner, otherwise do not save */
            if (OwnerName.Length > 0)
            {
                /* The record does not exist in the vehicle, create a new one. */
                if (Sqlid == 0)
                {
                    var currentPosition = new Vector3(0, 0, 0);
                    if (IsCreated()) currentPosition = vehicle.position;
                    // The record does not yet exist. (Car was just purchased?)
                    var query = new DBQuery($"INSERT INTO `{Table}` (owner, park_posX, park_posY, park_posZ, model) VALUES ('{OwnerName}', '{currentPosition.X}', '{currentPosition.Y}', '{currentPosition.Z}', '{(int)Model}')", QueryType.INSERT_OR_DELETE);
                    query.Execute();
                    Sqlid = (int)query.GetCommand().LastInsertedId;
                }
                else
                {
                    // TODO: Record exists, update
                }

                if (destroyFromWorld)
                    Dispose(true);
            }
        }

        public void Dispose(bool globalDispose = false)
        {
            if (IsCreated())
            {
                foreach (var occupant in Occupants)
                {
                    occupant.SendDebugMessage("This vehicle was disposed.");

                    occupant.OnExitVehicle(this);
                    occupant.RemoveFromVehicle();
                }
                API.shared.deleteEntity(vehicle);
                vehicle = null;
            }
            if (globalDispose)
            {
                this.Sqlid = 0;
                All.Remove(this);
            }
        }

        public void OnDeath()
        {
            IsDestroyed = true;
            Dispose(false);
        }

        public void SetDoorState(VehicleDoor door, bool open)
        {
            if (!IsCreated()) return;
            API.shared.setVehicleDoorState(vehicle.handle, (int)door, open);
        }

        public bool GetDoorState(VehicleDoor door)
        {
            return API.shared.getVehicleDoorState(vehicle.handle, (int) door);
        }

        public bool IsPlayerAuthorised(Player player)
        {
            if (OwnerName.Equals(player.Name)) return true;
            if (!Locked) return true;
            return false;
        }

        public void OnHealthChange(float value)
        {
            
        }

        public void ToggleIndicator(VehicleIndicators indicator)
        {
            if (!IsCreated()) return;
            if (indicator == VehicleIndicators.Left)
            {
                LeftIndicator = !LeftIndicator;
                API.shared.sendNativeToAllPlayers(GTANetworkServer.Hash.SET_VEHICLE_INDICATOR_LIGHTS, vehicle, (int)indicator, LeftIndicator);
            }
            else if (indicator == VehicleIndicators.Right)
            {
                RightIndicator = !RightIndicator;
                API.shared.sendNativeToAllPlayers(GTANetworkServer.Hash.SET_VEHICLE_INDICATOR_LIGHTS, vehicle, (int)indicator, RightIndicator);
            }
        }
    }

    public enum VehicleDoor
    {
        FrontDriver = 0,
        FrontPassenger = 1,
        RearDriver = 2,
        RearPassenger = 3,
        Hood = 4,
        Trunk = 5
    }
}
