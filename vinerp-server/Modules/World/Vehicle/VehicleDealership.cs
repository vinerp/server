﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;

namespace VineRP
{
    public class VehicleDealership
    {
        public static List<VehicleDealership> All = new List<VehicleDealership>();

        public static VehicleDealership CarDealership;

        public string Name;
        public Vector3 Position;
        public Vector3 VehicleShowroomPosition;
        public DealershipType DealershipType;
        public TextLabel TextLabel;
        /*                Model        Price */
        public List<Tuple<VehicleHash, int>> Vehicles = new List<Tuple<VehicleHash, int>>();

        public VehicleDealership(string name, Vector3 position, DealershipType dealershipType)
        {
            Name = name;
            Position = position;
            DealershipType = dealershipType;
            TextLabel = API.shared.createTextLabel($"~g~{Name}\n~w~Vehicle Type: {GetVehicleTypeName(DealershipType)}\nUse /vbuy",
                Position, 10f, 1f, false, 0);

            if (All.Count == 0)
            {
                /* Only register one event for all dealerships */
                Main.Instance.API.onPlayerExitVehicle += PlayerExitVehicle;
            }

            All.Add(this);
        }

        private void PlayerExitVehicle(Client client, NetHandle vehicleHandle)
        {
            var player = Player.GetPlayerFromClient(client);
            if (player.DealershipShowroomVehicle == null) return;

            if (vehicleHandle != player.DealershipShowroomVehicle.vehicle.handle) return;

            var vehicle = Vehicle.GetVehicleFromNetHandle(vehicleHandle);
            vehicle?.Dispose(true);

            player.Client.dimension = 0;
        }

        public void AddVehicle(VehicleHash model, int price)
        {
            if (!Vehicles.Exists(v => v.Item1 == model))
            {
                Vehicles.Add(new Tuple<VehicleHash, int>(model, price));
            }
        }

        public static string GetVehicleTypeName(DealershipType type)
        {
            switch (type)
            {
                case DealershipType.MOTORCYCLES:
                    return "Motorcycles";
                case DealershipType.CARS:
                    return "Cars";
                default:
                    return "Unknown";
            }
        }

        public static bool CMD_vbuy(Player player, string ps)
        {
            /* Retrieve the nearest dealership, the player must be within at least 3 units of a dealership */
            /*var nearestDealership =
                VehicleDealership.All.FirstOrDefault(v => v.Position.DistanceTo(player.Client.position) <= 3f);
            if (nearestDealership == null)
            {
                player.SendErrorMessage("You must be in a dealership to use this command!");
                return true;
            }*/
            var nearestDealership = VehicleDealership.All[0];

            string[] vehicleNames = new string[nearestDealership.Vehicles.Count];
            int[] vehiclePrices = new int[nearestDealership.Vehicles.Count];

            for (int i = 0; i < nearestDealership.Vehicles.Count; i++)
            {
                vehicleNames[i] = nearestDealership.Vehicles[i].Item1.ToString();
                vehiclePrices[i] = nearestDealership.Vehicles[i].Item2;
            }

            var menu = new Menu(player, 0, 0, MenuAnchor.MiddleLeft, "", nearestDealership.Name, true);

            foreach (var dealershipVehicle in nearestDealership.Vehicles)
            {
                menu.AddItem($"{dealershipVehicle.Item1.ToString()} - ${dealershipVehicle.Item2}",
                    $"The vehicle {dealershipVehicle.Item1.ToString()} costs ${dealershipVehicle.Item2}");
            }

            menu.OnItemSelect += (sender, args) =>
            {
                if (args.Index >= nearestDealership.Vehicles.Count) return;

                /* Put the player in its own, unique dimension */
                player.Client.dimension = player.Client.handle.Value;
                /* Clean up previous showroom vehicles */
                if (player.DealershipShowroomVehicle != null)
                {
                    if(player.DealershipShowroomVehicle.vehicle != null) API.shared.deleteEntity(player.DealershipShowroomVehicle.vehicle.handle);
                    player.DealershipShowroomVehicle = null;
                }
                player.DealershipShowroomVehicle = new Vehicle(nearestDealership.Vehicles[args.Index].Item1, player.Client.name);
                player.DealershipShowroomVehicle.Create(player.Client.position, new Vector3(), player.Client.dimension);
                API.shared.setEntityTransparency(player.DealershipShowroomVehicle.vehicle.handle, 200);

                player.PutInVehicle(player.DealershipShowroomVehicle, -1);

                /* Showing user the purchase confirmation menu */

                var confirmationMenu = new Menu(player, 0, 0, MenuAnchor.MiddleLeft, "",
                    $"{nearestDealership.Vehicles[args.Index].Item1.ToString()} - ${nearestDealership.Vehicles[args.Index].Item2}", true);

                confirmationMenu.AddItem("Exit", "Exit the dealership.");
                confirmationMenu.AddItem("Purchase",
                    $"Purchase {nearestDealership.Vehicles[args.Index].Item1.ToString()} for ${nearestDealership.Vehicles[args.Index].Item2}");

                confirmationMenu.OnItemSelect += (o, args2) =>
                {
                    if (args2.Index == 0)
                    {
                        // Exit the delearship
                        player.RemoveFromVehicle();
                        player.DealershipShowroomVehicle.Dispose(true);
                        player.DealershipShowroomVehicle = null;
                        player.Client.dimension = 0;
                    }
                    else if (args2.Index == 1)
                    {
                        if (player.Money < nearestDealership.Vehicles[args.Index].Item2)
                        {
                            player.SendErrorMessage("You don't have enough money to purchase this vehicle.");
                        }
                        else
                        {
                            /* Clean up previous showroom vehicles */
                            if (player.DealershipShowroomVehicle != null)
                            {
                                if (player.DealershipShowroomVehicle.vehicle != null) player.DealershipShowroomVehicle.Dispose(true);
                                player.DealershipShowroomVehicle = null;
                            }
                            var purchasedVehicle = new Vehicle(nearestDealership.Vehicles[args.Index].Item1, player.Client.name);
                            purchasedVehicle.Save();
                            player.Money -= nearestDealership.Vehicles[args.Index].Item2;
                            player.SendSuccessMessage($"Congratulations! You have purchased {nearestDealership.Vehicles[args.Index].Item1.ToString()} for ${nearestDealership.Vehicles[args.Index].Item2}");
                            player.Client.dimension = 0;
                        }
                    }
                };

                confirmationMenu.Show();
            };

            menu.Show();
            //player.TriggerEvent("modules_vehicle_dealership_menu_show", nearestDealership.Name, vehicleNames, vehiclePrices, (int)nearestDealership.DealershipType);
            return true;
        }

        public static void Init()
        {
            Log.Info("Creating vehicle dealerships...");
            CarDealership = new VehicleDealership("Premium Deluxe Motorsports",
                new Vector3(-42.55413, -1092.99, 26.42235), DealershipType.CARS)
            {
                VehicleShowroomPosition = new Vector3(-38.73658, -1100.82, 26.42235)
            };
            CarDealership.AddVehicle(VehicleHash.Adder, 500000);
            CarDealership.AddVehicle(VehicleHash.Elegy, 243000);
            CarDealership.AddVehicle(VehicleHash.Zentorno, 964000);
            CarDealership.AddVehicle(VehicleHash.Faggio, 5400);

            /* Note: Registering the event on API.shared does not work */
            Main.Instance.API.onClientEventTrigger += (sender, name, arguments) =>
            {
                if (name.Equals("modules_vehicle_dealership_buy_select"))
                {
                    var index = (int)arguments[0];
                    var dealership = VehicleDealership.All.FirstOrDefault(d => d.DealershipType == ((DealershipType)(int)arguments[1]));
                    if (dealership != null)
                    {
                        if (index < dealership.Vehicles.Count)
                        {
                            var player = Player.GetPlayerFromClient(sender);
                            if (player == null) return;
                            /* Put the player in its own, unique dimension */
                            player.Client.dimension = player.Client.handle.Value;
                            /* Clean up previous showroom vehicles */
                            if (player.DealershipShowroomVehicle != null)
                            {
                                if(player.DealershipShowroomVehicle.vehicle != null) API.shared.deleteEntity(player.DealershipShowroomVehicle.vehicle.handle);
                                player.DealershipShowroomVehicle = null;
                            }
                            player.DealershipShowroomVehicle = new Vehicle(dealership.Vehicles[index].Item1, player.Client.name);
                            player.DealershipShowroomVehicle.Create(dealership.VehicleShowroomPosition, new Vector3(), player.Client.dimension);
                            API.shared.setEntityTransparency(player.DealershipShowroomVehicle.vehicle.handle, 200);

                            player.PutInVehicle(player.DealershipShowroomVehicle, -1);
                            // trigger new menu

                            player.TriggerEvent("modules_vehicle_dealership_buy_menu_show", (int)dealership.DealershipType, dealership.Vehicles[index].Item1.ToString(), dealership.Vehicles[index].Item2, index);
                        }
                        Log.Debug("The dealership was found");
                    }
                    Log.Debug($"Selected index is {(int)arguments[0]}");
                    Log.Debug($"The dealership id is {(int)arguments[1]}");
                }
                else if (name.Equals("modules_vehicle_dealership_buy_menu_purchase"))
                {
                    var dealership = VehicleDealership.All.FirstOrDefault(d => d.DealershipType == ((DealershipType)(int)arguments[0]));
                    if (dealership != null)
                    {
                        // The index of the vehicle inside the dealership
                        var index = (int)arguments[1];

                        // Ensure that the selected vehicle exists to prevent NullPointerException
                        if (index < dealership.Vehicles.Count)
                        {
                            var player = Player.GetPlayerFromClient(sender);
                            if (player == null) return;

                            if (player.Money < dealership.Vehicles[index].Item2)
                            {
                                player.SendErrorMessage("You don't have enough money to purchase this vehicle.");
                            }
                            else
                            {
                                /* Clean up previous showroom vehicles */
                                if (player.DealershipShowroomVehicle != null)
                                {
                                    if (player.DealershipShowroomVehicle.vehicle != null) API.shared.deleteEntity(player.DealershipShowroomVehicle.vehicle.handle);
                                    player.DealershipShowroomVehicle = null;
                                }
                                var purchasedVehicle = new Vehicle(dealership.Vehicles[index].Item1, player.Client.name);
                                purchasedVehicle.Save();
                                player.SendSuccessMessage($"Congratulations! You have purchased {dealership.Vehicles[index].Item1.ToString()} for ${dealership.Vehicles[index].Item2}");
                            }
                        }
                    }
                }
            };
        }
    }

    public enum DealershipType
    {
        CARS = 1,
        MOTORCYCLES = 2
    }
}
