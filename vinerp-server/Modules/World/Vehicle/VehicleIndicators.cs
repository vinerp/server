﻿using System;

namespace VineRP
{
    public enum VehicleIndicators
    {
        Right = 0,
        Left = 1
    }
}