﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkServer;
using GTANetworkShared;

namespace VineRP
{
    public class VehicleRent
    {
        public const int RENT_COST = 500;
        public const VehicleHash RENT_VEHICLE = VehicleHash.Stanier;

        public static List<Vector3> RentLocations = new List<Vector3>()
        {
            /* Airport parking lot */
            new Vector3(-1043.976, -2660.698, 13.40652)
        };

        /* This stores the vehicle spawn locations for vehicle rent at the airport
           The format is: Rent Location ID, vehicle spawn position, vehicle spawn rotation
        */
        public static List<Tuple<uint, Vector3, Vector3>> VehicleSpawns = new List<Tuple<uint, Vector3, Vector3>>()
        {
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1029.051, -2660.885, 13.40602), new Vector3(-0.1195551, 0.05163248, 150.3763)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1032.003, -2659.262, 13.40666), new Vector3(-0.07106604, -0.02288707, 149.2321)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1034.904, -2657.44, 13.40656), new Vector3(-0.08762408, 0.009590775, 151.0981)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1037.778, -2655.645, 13.40668), new Vector3(-0.08709125, -0.0003153292, 150.2879)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1040.782, -2653.812, 13.40674), new Vector3(-0.1641272, -0.001983349, 151.0137)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1043.583, -2652.262, 13.4065), new Vector3(-0.1575234, 0.001469184, 150.9921)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1046.504, -2650.628, 13.40668), new Vector3(-0.08191226, 0.002646077, 148.3651)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1055.867, -2654.669, 13.40622), new Vector3(-0.1303104, 0.005378612, -73.09814)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1054.792, -2657.761, 13.40667), new Vector3(-0.08068993, 0.01386586, -71.14441)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1053.68, -2660.488, 13.406), new Vector3(-0.1179156, 0.01384459, -69.47813)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1052.48, -2663.793, 13.40645), new Vector3(-0.06808002, 0.0004582379, -64.0201)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1050.626, -2666.406, 13.40673), new Vector3(-0.1519826, 0.0002776698, -59.41957)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1048.467, -2669.284, 13.40628), new Vector3(-0.132766, 0.0003124344, -50.11317)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1046.472, -2671.547, 13.40655), new Vector3(-0.09902339, 0.001470098, -45.95504)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1044.314, -2673.829, 13.40631), new Vector3(-0.134282, 0.01667966, -41.5614)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1041.838, -2676.486, 13.40628), new Vector3(-0.1273563, 0.04545397, -33.5324)),
            new Tuple<uint, Vector3, Vector3>(0, new Vector3(-1038.892, -2677.982, 13.40671), new Vector3(-0.1604855, 0.01823852, -30.77979))
        };

        public static void Init()
        {
            foreach (var rentLocation in RentLocations)
            {
                API.shared.createTextLabel($"Vehicle Rent Location\nUse /rentvehicle\nRent cost: ${RENT_COST}", rentLocation, 15f, 1f, false);
            }
        }

        public static bool CMD_rentvehicle(Player player, string ps)
        {
            var closestRentLocation = RentLocations.FirstOrDefault(r => r.DistanceTo(player.Client.position) <= 5f);
            if (closestRentLocation == null)
            {
                player.SendErrorMessage("You are not near a vehicle rent location.");
                return true;
            }

            if (player.Money < RENT_COST)
            {
                player.SendErrorMessage("You do not have enough money to rent a vehicle.");
                return true;
            }

            /* If the player already has a rental vehicle, destroy it. */
            if (player.RentVehicle != null)
            {
                player.RentVehicle.Dispose(true);
                player.SendInfoMessage("Your previous rental vehicle was towed back.");
            }

            var randomSpawnLocation = VehicleSpawns[Random.GetInt(0, VehicleSpawns.Count)];
            player.RentVehicle = new Vehicle(RENT_VEHICLE, player.Name);
            player.RentVehicle.Create(randomSpawnLocation.Item2, randomSpawnLocation.Item3);
            player.PutInVehicle(player.RentVehicle);

            player.Money -= RENT_COST;
            player.SendSuccessMessage("You have rented a vehicle for $" + RENT_COST);
            return true;
        }

    }
}