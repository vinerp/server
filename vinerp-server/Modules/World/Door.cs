﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using VineRP;

namespace VineRP
{
    public class Door
    {
        public const string Table = "doors";
        public static List<Door> All = new List<Door>();

        public int ModelHash;
        public Vector3 Position;
        public bool Locked;
        public float State;
        public SphereColShape ColShape;

        public Door(int modelHash, Vector3 position)
        {
            Position = position;
            ModelHash = modelHash;
            Locked = false;
            State = 0f;

            ColShape = API.shared.createSphereColShape(position, 35f);
            ColShape.setData("DOOR_OBJECT", this);
            ColShape.setData("IS_DOOR_TRIGGER", true);


            Main.Instance.API.onEntityEnterColShape += OnEntityEnterColShape;

            All.Add(this);
        }

        private void OnEntityEnterColShape(ColShape colShape, NetHandle entity)
        {
            if (colShape == null) return;
            if (API.shared.getEntityType(entity) != EntityType.Player) return;
            if (colShape.getData("IS_DOOR_TRIGGER") == null || !colShape.getData("IS_DOOR_TRIGGER")) return;
            var player = Player.GetPlayerFromNetHandle(entity);

            player.SendDebugMessage("You have entered a door colshape");

            Door door = colShape.getData("DOOR_OBJECT");

                API.shared.deleteObject(player.Client, door.Position, door.ModelHash);
            float heading = 0f;
            if (door.State != null) heading = door.State;

            API.shared.sendNativeToPlayer(player.Client, GTANetworkServer.Hash.SET_STATE_OF_CLOSEST_DOOR_OF_TYPE,
                door.ModelHash, door.Position.X, door.Position.Y, door.Position.Z, door.Locked, heading, false);

        }

        public static void LoadAll()
        {
            Log.Info("Loading doors...");
            var query = new DBQuery($"SELECT * FROM `{Table}`", QueryType.QUERY);
            query.Execute();
            if (query.GetResult().HasRows)
            {
                while (query.GetResult().Read())
                {
                    var door = new Door(query.GetResult().GetInt32("modelHash"), new Vector3(query.GetResult().GetFloat("posX"), query.GetResult().GetFloat("posY"), query.GetResult().GetFloat("posZ")));
                    All.Add(door);
                }
            }
            query.Close();
        }
    }
}
