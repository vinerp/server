﻿using GTANetworkShared;

namespace VineRP
{
    public class Marker
    {
        private Player _player;
        private Vector3 _position;
        private Color _color;
        private int _type;
        private int _id;

        public bool IsCreated;

        public Marker(Player player, Vector3 position, Color color, int type = 1)
        {
            _player = player;
            _position = position;
            _color = color;
            _type = type;
            _id = player.CurrentMarkerCounter++;

            player.TriggerEvent(PlayerEvents.MARKER_CREATE, _id, _type, _position, (int)color.R, (int)color.G,
                (int)color.B, (int)color.A);

            IsCreated = true;
        }

        public void Dispose()
        {
            if (!IsCreated) return;
            _player.TriggerEvent(PlayerEvents.MARKER_DESTROY, _id);
            IsCreated = false;
        }

        public void SetColor(Color color)
        {
            if (!IsCreated) return;
            _player.TriggerEvent(PlayerEvents.MARKER_SET_COLOR, _id, (int)color.R, (int)color.G, (int)color.B,
                (int)color.A);
        }
    }
}