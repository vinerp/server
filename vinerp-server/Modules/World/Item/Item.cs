﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VineRP
{
    public class Item
    {
        public static List<Item> All = new List<Item>();

        /* Items */
        public static Item[] Fish = new Item[FactionFishing.fishNames.Length];
        public static Item FishingRod;

        public string Name;
        // ID is only used for storing the inventory in the database
        public int ID;
        public string Unit;
        public ItemType ItemType;
        public dynamic MaxStackableSize;

        public Item(string name, dynamic maxStackableSize, ItemType itemType = ItemType.Generic)
        {
            Name = name;
            ItemType = itemType;
            MaxStackableSize = maxStackableSize;
            // Add 1 so that ID starts at 1
            ID = All.Count + 1;

            All.Add(this);
        }

        public static Item GetItemById(int id)
        {
            return All.FirstOrDefault(i => i.ID == id);
        }

        public static void Init()
        {
            Log.Info("Creating inventory items...");
            // Create fish
            for (int f = 0; f < FactionFishing.fishNames.Length; f++)
            {
                Fish[f] = new Item(FactionFishing.fishNames[f], ItemType.Fish)
                {
                    Unit = "lbs"
                };
            }


            FishingRod = new Item("Fishing Rod", 1);
            Log.Debug($"The unique id for the fishing rod is - {FishingRod.ID}");
        }
    }

    public enum ItemType
    {
        Generic,
        Fish,
        Sidearm,
        Rifle,
        Shotgun
    }
}
